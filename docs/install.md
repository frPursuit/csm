# CSM installation

This tutorial covers how to install CSM and its dependencies.

## Install CSM

CSM can be installed from this repository:

    pip install -e <folder>

Where `<folder>` is the folder that contains the `setup.py` script.

## Install Docker

[Docker](https://www.docker.com/) is required to run containers.

Please follow [this tutorial](https://docs.docker.com/engine/install/) to install Docker server. For instance, on Debian, [here](https://docs.docker.com/engine/install/debian/)'s how to install Docker.

## Install nginx

An [nginx](https://www.nginx.com/) reverse proxy is required for services to be exposed via HTTP.

On debian, it can be installed using the following command:

    apt install nginx

## Install certbot

[Certbot](https://certbot.eff.org/) is requried to generate SSL certificates. It can be installed with the following command:

    apt install python3-certbot

In addition, a [plugin](https://eff-certbot.readthedocs.io/en/stable/using.html#getting-certificates-and-choosing-plugins) needs to be installed depending on how the certificates are to be validated.

## Install PostgreSQL

[PostgreSQL](https://www.postgresql.org/) is required to provide databases to services.

On debian, it can be installed with the following command:

    apt install postgresql

After being installed, PostgreSQL must be made available to the containers. First, make sure the service listens on every interface by modifying `/etc/postgresql/<postgres version>/main/postgresql.conf` as follows:

```diff
#------------------------------------------------------------------------------
# CONNECTIONS AND AUTHENTICATION
#------------------------------------------------------------------------------

# - Connection Settings -

-#listen_addresses = 'localhost'         # what IP address(es) to listen on;
+listen_addresses = '*'                  # what IP address(es) to listen on;
```

Finally, authentication must be authorized from the container's IP addresses. To allow authentication from all IPv4 and IPv6 addresses by modifying `/etc/postgresql/<postgres version>/main/pg_hba.conf` as follows:

```diff
# TYPE  DATABASE        USER            ADDRESS                 METHOD

# "local" is for Unix domain socket connections only
local   all             all                                     peer
-# IPv4 local connections:
-host    all             all             127.0.0.1/32            scram-sha-256
-# IPv6 local connections:
-host    all             all             ::1/128                 scram-sha-256
+# IPv4 connections:
+host    all             all             0.0.0.0/0               scram-sha-256
+# IPv6 connections:
+host    all             all             ::/0                    scram-sha-256
```

**Warning: It is advised to set up a firewall to prevent external connections to the database.**
