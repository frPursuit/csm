# Environment variables

Each container the service is composed of can be configured via environment variables. Environment variables come from two sources:

- Some are configured in the [service's specification](./service_spec.md)
- Some are configured in the [service's configuration](./config.md#service-configuration)

*Note: Variables defined in the service's configuration can override those defined in its specification.*

## Context variables

Context variables can be used to parametrize environment variables with a [Jinja2](https://palletsprojects.com/p/jinja/) syntax.

- Some context variables are exposed by the service's components. Please refer to the [components list](./service_spec.md) to know more about these.

- The [service configuration](./config.md) is also exposed as the `config` context variable: a field called `bar` in a section `foo` can be accessed
with the `config.foo.bar` variable. (*Note: The sections whose name contain a `.` (such as the `env.*` sections) are not part of the `config` context variable.*)

- Finally, [secrets](./secrets.md) are available under the `secrets` context variable.
