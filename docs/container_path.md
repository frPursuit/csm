# Container path

A *container path* is a string used to refer to a path inside of a particular container.

It is in the format `container:local_path`, or optionally `container:local_path:metadata`.

## Refer to a path relative to the root of a container

A container path can be used to refer to a path relative to the root of a container.

In that case, it is in the format `container:/path/inside/the/container`

*Note: When using these container paths, the container-local path **cannot** be inside a volume. This is due to the inability to create volumes inside existing volumes.*
*To refer to paths that are inside volumes, please use container paths as specified below.*

## Refer to a path relative to the root of a volume

To refer to paths that are inside a volume, it is necessary to use a path that is relative to the root of said volume.

In that case, the container path is in the format `#volume:path/inside/the/volume`.

*Note: The internal path **can** start with a digit. Container paths are only identified as referring to TCP ports when they are relative to containers*
*(ie: when they don't start with the character `#`)*

## Refer to a TCP port inside the container

To refer to a TCP port inside the container, the syntax `container:tcp_port` is used, when `tcp_port` naturally starts with a digit.

## Metadata

Metadata can be associated with the container path using the `container:local_path:metadata` format.

It is usually used to add mouting options to the associated volume.
