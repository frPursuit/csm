# Service images

A CSM service can be exported into a *Service image* for backup or migration purposes.

A service image is an encrypted and compressed file, usually ending with the `.svi` file extension.

*Note: the image encryption key is generated for the first time when a service is exported.*
*It can also be imported from other instances of CSM.*

## SVI file format

An SVI file is an encrypted compressed [TAR archive](https://en.wikipedia.org/wiki/Tar_(computing)). It is created by:

- Packaging a directory into a TAR archive
- Compressing the TAR archive using the [GZIP](https://www.gnu.org/software/gzip/) tool.
- Encrypting the compressed archive using the [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) algorithm.

Therefore, an SVI file is composed of:

- Three (ASCII encoded) magic bytes: `S`, `V`, `I`
- An ASCII encoded character representing the format's version: `1`
- The encryption IV (16 bytes)
- The encrypted & compressed tarfile

## Packaged directory structure

The packaged directory contains:

- A `service.yml` file, which is the [specification](./service_spec.md) of the service
- A `secrets.yml` file containing the service's [secrets](./secrets.md)
- A `data` directory which contains the service's data:
    - Data volumes in the `volumes` directory
    - Component-specific data in a directory named after the component (ex: `databases`)
- A `config` directory which contains the service's [configuration](./config.md)
