# Containers

## Container specification

A *container specification* is a YAML object that defines the properties of a container. Its format is based on the values of the dictionary specified in [Docker compose's `services` section](https://docs.docker.com/compose/compose-file/05-services/), with the following modifications:

- In the `volumes` section, only named volumes can be used (not mountpoints). These volumes define persistent [data volumes](./volumes.md#volume-types).
- The `networks` section is ignored. Networking is handled by [CSM's IPAM](./ipam.md).
- The `env_file` section is ignored. Environment variables are specified either by the [service's spec](./service_spec.md), or by the [service's configuration](./config.md#service-configuration).
- The `environment` section should be a dictionary instead of a list of strings. [Context variables](./environment.md#context-variables) can be used here, with a [Jinja2](https://palletsprojects.com/p/jinja/) syntax
- The `restart` parameter is controlled by CSM (cf: [service specification](./service_spec.md))

*Note: In addition to the environment variables specified in the `environment` section, variables specified in the [service's configuration](./config.md#service-configuration) are also passed to containers.*

## Container set

A *container set* is a dictionary that defines a set of named containers represented by their [specification](#container-specification).
