# IP address management

CSM's IPAM system is used to associate predictable IPs to service containers, in the following way:

- A prefix called `ip_pool` is configured in the IPAM configuration and delegated to CSM.
- For each service, a free network called `service_network` is taken out of the `ip_pool`. The size of the network is specified by the `service_network_size` parameter.
- The first usable IP of the network is the network gateway (ie: the IP of the host interface in that network)
- All other IP addresses are statically associated to the service's containers on install. **There is no guarantee that a container IP won't change if a service is updated or reinstalled**, which is why it's always better to refer to containers using their *hostnames*.

## Container hostnames

When a service is installed, its containers are given an IP. These IPs are then associated with hostnames, hardcoded in the local `/etc/hosts` file, in the following format:

    <container>.<service>.svc.local

Where:

- `<container>` is the name of the container
- `<service>` is the name of the service
