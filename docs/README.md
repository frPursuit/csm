# CSM documentation

CSM is a tool that allows containerized services to be easily installed, managed, and removed from Linux servers.

Under the hood, CSM uses:

- [Docker](https://docker.com/) containers to provide the service's execution environment
- Database providers (such as [PostgreSQL](https://www.postgresql.org/)) to provision databases for the services
- An [nginx](https://www.nginx.com/) reverse proxy to expose the services
- [certbot](https://certbot.eff.org/) to manage [Let's Encrypt](https://letsencrypt.org/) certificates (in order to expose services with HTTPS)
- [systemd timers](https://wiki.archlinux.org/title/systemd/Timers) to have the services periodically execute actions

CSM services can also be [exported](./image.md) for backup or migration purposes.

## What is a CSM service composed of?

A CSM service is composed of:

- A [Service specification](./service_spec.md) that describes its different components
- [Configuration files](./config.md#service-configuration) that contain global and container-specific parameters that can be edited by the user
- [Secrets](./secrets.md), such as keys or passwords
- Variable data (databases and persistent volumes) meant to be modified and reused by the service

## How are CSM services identified?

When a CSM service is installed, it is identified by an *instance ID*. This is the ID that should be used when interacting with the installed service on a particular system.

When updating the service, a *service ID* is used to ensure the old service corresponds to an older version of the newer service. This service ID is defined in the [service specification](./service_spec.md).

*Note: When not specified during installation, the instance ID defaults to the service ID.*

## How to install CSM services?

A CSM service can be installed or updated using a [Service specification](./service_spec.md).

## Where are CSM files stored?

CSM files are stored in several locations depending on their usage, in accordance to the [Linux FHS](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html).

- The [specifications](./service_spec.md) of installed services are stored in the `/usr/lib/csm/services` directory.
    - Only files ending with `.yml` or considered to avoid duplicates. The filename corresponds to the instance ID.
- The [configuration](./config.md) of CSM is located in the `/etc/csm` directory
- The variable data (ie: [volumes](./volumes.md#volume-types) and [secrets](./secrets.md)) of services are stored in the `/var/lib/csm/services` directory.
    - There is a subfolder per installed service (named after the instance ID)
