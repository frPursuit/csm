# Service specification

A Service specification describes the components of a service.

It is usually represented as a YAML object, and contains the properties listed in this document.

*Note: Only the `id` property is mandatory.*

*Note 2: Most components listed in this document are named. Such names can only contain ASCII alphanumeric characters, and the characters `.`, `-` or `_`*

## `id` - The service identifier

A string that identifies the service (as in, the application defined by the specification).
Used when updating a service.
Usually in `snake_case`.

**Not to be confused with the *instance* identifier which identifies the particular instance of the service, and usually corresponds to the name of the specification file.**

## `containers` - The containers that are necessary for the service to run properly

This section contains a list of [Docker](https://www.docker.com/) named containers described by their own [specification](./containers.md#container-specification).

These containers are configured to automatically restart unless explicitly stopped (ie: the `restart` mode is set to `unless-stopped`).

### Example:

```yml
containers:
  main:
    image: "alpine"
```

## `databases` - The databases used by the service

Each database is named, and has a value corresponding to its database type. [Click here](./databases.md#databases-types) to see a list of all currently supported database types.

When a database is provisioned, information required to connect to it is available as context variables:

| Name                        | Description                                  |
|-----------------------------|----------------------------------------------|
| `databases.<name>.host`     | The host where the database is available     |
| `databases.<name>.name`     | The local name of the database               |
| `databases.<name>.user`     | The user used to connect to the database     |
| `databases.<name>.password` | The password used to connect to the database |

*Where `<name>` is the name of the database.*

### Example:

```yml
databases:
  main: postgresql
```

## `timers` - The timers used by the services

Timers can be used to periodically execute tasks on a specific container.

These tasks can either be executed on an exisitng container, or on a dedicated container

Each timer is named, and must contain the following properties:

**Note: to avoid IPAM clashes, containers and timers should not have the same name.**

### Specify the timer's inverval

The timer's interval must be specified using the `at` property. It follows the syntax of the [`OnCalendar` property from systemd](https://www.freedesktop.org/software/systemd/man/systemd.timer.html#OnCalendar=).

### Execute a task on an existing container

If the task needs to be executed on an existing container, this section should contain the following properties:

- `container`: The name of the container where the task is to be executed 
- `command`: An array repsenting the command that is to be executed on the container

*Note: This mode is identified by the presence of the `container` property inside this section.*

### Execute a task on a dedicated container

If the task needs to be executed on a dedicated container, this section should define a container with a standard [specification](./containers.md#container-specification) (excluding the `at` property).

In that case, the resulting container is identified by the timer's name.

This container is then configured to never automatically restart (ie: the `restart` mode is set to `no`).

### Example:

```yml
timers:
  task_on_existing:
    at: "*:0/30"
    container: my_container
    command: ["echo", "hello world"]
  task_on_dedicated:
    at: "*:0/30"
    image: alpine
    command: ["echo", "hello world"]
```

## `http` - The service's HTTP configuration

This section describes the parts of the service that need to be exposed via HTTP.

It contains a dictionary where each entry is an endpoint:

- The key is the relative path of the endpoint, and must start with a `/`
- The value is a [container path](./container_path.md) identifying the content of the endpoint. If the local path starts with a digit, it is assumed to be a TCP port. Otherwise, it is handled as a filesystem path either inside the container or inside a volume, as specified in the [container path specification](./container_path.md).

*Note: When a filesystem path that is not inside an existing volume is used, a new volume is created.*
*Its mounting options can be specified via the `metadata` part of the container path.*
*The resulting folder inside the container will initally be empty, and should be populated by the container.*

### Example:

```yml
http:
  "/": "main_app:8000"
  "/static": "main_app:/app/static"
```

## `secrets` - The service's secrets

In this section, additional [secrets](./secrets.md) that are necessary for the service to work properly can be defined.

Each entry corresponds to a secret. The entry's key is the name of the secret, and the entry's value is the length of the secret.

*Note: Secrets that are automatically generated by CSM only use alphanumeric characters.*

### Example:

```yml
secrets:
  my_secret: 32
  password: 12
```

## `config` - The service's configuration

The default [configuration](./config.md#service-configuration) of the service. It is a dictionary where:

- Keys are configuration sections
- Values are dictionaries themselves, representing the options that should be present within the specified config section

### Example

```yml
config:
  env.first:
    first_option: true
    second_option: 42d
  env.second:
    third_option: false
```

## `config_volumes` - Configuration volumes

This section can be used to define [configuration volumes](./volumes.md#volume-types).
Such volumes can be used to share configuration files with a container.

Default configuration files can be downloaded automatically when the service is installed.

It contains a dictionary, where the keys are the names of the volumes and the values are objects where:

- The `path` property is a [container path](./container_path.md) relative to the root of a container.
- The `files` optional property is another dictionary used to specify the files that need to be downloaded when the service is installed

*Note: Configuration volumes are mounted with the `z,ro` options by default to give the user full control over configuration files,*
*unless explicit mounting options are specified via the `metadata` part of the container path.*

*Note 2: `service.cfg` **is an invalid config volume name**, as it clashes with the [`service.cfg` configuration file](./config.md#service-configuration)*

### Example:

```yml
config_volumes:
  my_config_volume:
    path: container:/etc/app
    files:
      "path/file1": "https://webserver.example/file1"
      "path/file2": "https://webserver.example/file2"
      "path/file3": "https://webserver.example/file3"
```
