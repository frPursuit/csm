# CSM configuration

The CSM configuration files are located in the `/etc/csm` directory.

## Global configuration

The CSM global configuration file is an INI file located at `/etc/csm/global.cfg`. It contains the following global CSM parameters:

### In the `[network]` section

- `ip_pool` - The pool of IP addresses used by CSM's [IPAM](./ipam.md)
- `service_network_size` - The size of the subnet delegated to a CSM service

### In the `[db-providers]` section

This section specify the [database providers](./databases.md) to use for each type of database.

### In the `[letsencrypt]` section

This section contains information required to generate [Let's Encrypt](https://letsencrypt.org/) certificates using [certbot](https://certbot.eff.org/):

- `email`: The email to give to Let's Encrypt when requesting a certificate
- `certbot_arguments`: The arguments to give to certbot when requesting a certificate (namely to choose a request method)

### Example

```ini
[network]
ip_pool=10.44.0.0/16
service_network_size=24

[db-providers]
postgresql=local

[letsencrypt]
email=me@website.example
certbot_arguments=--dns-ovh --dns-ovh-credentials /etc/letsencrypt/ovh.ini
```

## Service configuration

Services also have their own local configuration that can be modified to alter service-specific paramters.

The service-local configuration is an INI file located at `/etc/csm/services/<service>/service.cfg`, where `<service>` is the instance ID of the service.

*Note: In addition to this standard configuration, services can also expose custom [configuration volumes](./volumes.md#volume-types)*

### HTTP configuration

The following parameters can be used to tune a service's [HTTP component](./service_spec.md#http---the-services-http-configuration) in the `[http]` section:

- `domain` - The domain at which the service will be accessible
- `tls` (Optional) - Whether HTTPS should be used instead of HTTP. Default: `false`
- `upload_limit` (Optional) - The upload limit associated with the service. Default: `1M`

**Note: If TLS is enabled and the domain needs to be changed, the old domain needs to be manually removed using the command `certbot delete --cert-name <domain>`**

#### Example

```ini
[http]
domain = service.example
tls = true
upload_limit = 100M
# The service is then accessible at https://service.example
```

### Environment configuration

Extra [environment variables](./environment.md) can be passed to containers in the `[env.<container>]` section, where `<container>` is the name of a container.

#### Example

```ini
[env.ct1]
env1 = value1
env2 = value2

[env.ct2]
env3 = "{{ this_is.a_context.variable }}"
env4 = value4
```

### Service-specific configuration

Services can also define their own configuration in the `config` section of their [specification](./service_spec.md#config---the-services-configuration).

This configuration is then made available to the service as [context variables](./environment.md#context-variables), used to parametrize environment variables passed to the containers.
