# Database configuration

## Databases types

The database types that are currently supported are:

### `postgresql` - PostgreSQL database

Database using the [PostgreSQL](https://www.postgresql.org/) backend.

## Database providers

Database providers are used to provision databases when installing new services.

The database providers that are currently supported are:

| Provider name | Supported database types |
|---------------|--------------------------|
| `local`       | `postgresql`             |

*Note: When using the `local` provider, the database server must be configured to listen on all interfaces.*
