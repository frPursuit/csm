# Volumes in CSM

## Volume types

Volumes created with CSM can be of several types:

- *Data volumes* are stored in the `/var/lib/csm/services/<service>/data` directory. They contain persistent data that should be backed up and included in migrations
- *Volatile volumes* are stored in the `/var/lib/csm/services/<service>/volatile` directory. They are created purely to expose files on the host
- *Configuration volumes* are stored in the `/etc/csm/services/<service>` directory. They are created to expose configuration files to the user. A configuration volume **cannot be named `service.cfg`**, as its name would clash with the service's [local configuration](./config.md).

*Note: In the above paths, <service> is the service's instance ID.*

By default, all volumes are empty. Therefore, they need to be filled either by the container itself, or by CSM via the `files` section of the [service's spec](./service_spec.md).

## Volume options

All standard [Docker bind mount options](https://docs.docker.com/storage/bind-mounts/) are accepted and passed to Docker.

Extra options managed by CSM can also be specified:

- `user=<user>` specifies the user or UID the volume should belong to (when not specified, defaults to `root`)
- `group=<group>` specifies the group or GID the volume should belong to (when not specified, defaults to the `user` option)
