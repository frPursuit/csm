# Containerized Service Manager

CSM is a tool that allows containerized services to be easily installed, managed, and removed from Linux servers.

Under the hood, CSM uses:

- [Docker](https://docker.com/) containers to provide the service's execution environment
- Database providers (such as [PostgreSQL](https://www.postgresql.org/)) to provision databases for the services
- An [nginx](https://www.nginx.com/) reverse proxy to expose the services
- [certbot](https://certbot.eff.org/) to manage [Let's Encrypt](https://letsencrypt.org/) certificates (in order to expose services with HTTPS)
- [systemd timers](https://wiki.archlinux.org/title/systemd/Timers) to have the services periodically execute actions 

Services are distributed and installed using [service speficiations](./docs/service_spec.md). They can also be exported for backup or migration purposes using [service images](./docs/image.md).

## Usage

```
usage: csm [-h] [-v] [-V] action ...

optional arguments:
  -h, --help     show this help message and exit
  -v, --verbose  Increase the output's detail level
  -V, --version  display the program's version

action:
  action
    compose      Execute a docker compose command
    exec         Execute a command inside a container
    export       Export a service and its data
    exportkey    Export the CSM image key
    extract      Extract a service image
    import       Import a service and its data
    importkey    Import a CSM image key
    install      Install a service from a service specification
    list (ls)    List installed services
    logs         Display the logs of a service
    remove       Remove an installed service
    repair       Repair an installed service
    restart      Restart a service
    start        Start a service
    status       Check the status of a service
    stop         Stop a service
    timer        Display the status or logs of a timer
```

## Documentation

- [Project documentation](./docs/README.md)
- [Installation instructions](./docs/install.md)
- [Basic specification examples](./examples/)
- [Real-life service specifications](https://gitlab.com/frPursuit/csm-deployments)

## License

This project is licensed under the [GNU GPLv3 license](./LICENSE).
