from setuptools import find_packages, setup
from pathlib import Path

directory = Path(__file__).parent

setup(
    name="csm",
    version="0.2.0",
    packages=find_packages(),
    include_package_data=True,
    package_data={"csm": ["templates/*"]},
    entry_points={
        "console_scripts": [f"csm=csm.main:main"],
    },
    author="Pursuit",
    author_email="fr.pursuit@gmail.com",
    description="Containerized service manager",
    long_description=(directory / "README.md").read_text(encoding="utf-8"),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/frPursuit/csm",
    license="GNU General Public License v3 (GPLv3)",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
    python_requires=">=3.8",
    install_requires=[
        "pursuitlib>=0.4.0",
        "requests>=2.25.1",
        "PyYAML>=5.3.1",
        "jinja2>=2.11.3",
    ]
)
