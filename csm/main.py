import traceback
from argparse import ArgumentParser

import csm
from csm.config import Config
from csm.hosts import Hosts
from csm.registry import Registry
from pursuitlib.console import console

# Exit error codes
ERROR_ACTION_EXCEPTION = 10


def main():
    Registry.initialize()
    Config.initialize()
    Hosts.initialize()

    parser = create_parser()
    args = parser.parse_args()
    Config.verbose = args.verbose

    action = Registry.get_action(args.action)
    try:
        action.execute(args)
    except Exception as e:
        console.write_err_line(f"{e}")

        if Config.verbose:
            traceback.print_exc()

        exit(ERROR_ACTION_EXCEPTION)


def create_parser() -> ArgumentParser:
    parser = ArgumentParser()

    parser.add_argument("-v", "--verbose", action="store_true", help="Increase the output's detail level")
    parser.add_argument("-V", "--version", help="display the program's version", action="version", version=f"Containerized Service Manager {csm.__version__} - by Pursuit")

    action = parser.add_subparsers(title="action", dest="action", metavar="action", required=True)
    for act in Registry.get_actions():
        act.register_parser(action)

    return parser


if __name__ == "__main__":
    main()
