from csm.components.metacomponent import MetaComponent


class ConfigComponent(MetaComponent):
    def __init__(self):
        super().__init__("config")
