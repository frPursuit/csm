import os
from typing import TYPE_CHECKING, Dict, List, Optional

from pursuitlib.iterators import citer

from csm.components.component import Component
from csm.container.containerset import ContainerSet
from csm.container.restartmode import RestartMode
from csm.states.containerstate import ContainerState
from csm.states.servicestate import ServiceState
from csm.states.state import State
from csm.volume.volume import Volume
from pursuitlib.console.table import Table
from pursuitlib.console.textcolors import TextColors
from pursuitlib.console.textcomponent import TextComponent

if TYPE_CHECKING:
    from csm.service.service import Service


class ContainersComponent(Component):
    CONTAINERS_FILE = "containers.yml"
    MAIN_NETWORK_NAME = "main"

    def __init__(self):
        super().__init__("containers")

    def get_containers_path(self, service: "Service"):
        return os.path.join(service.get_data_directory(), ContainersComponent.CONTAINERS_FILE)

    def get_volume_dir(self, service: "Service", name: str):
        return os.path.join(service.get_data_directory(), "volumes", name)

    def list_changes(self, service: "Service", containers: Optional[dict], update_from: Optional[dict]) -> List[TextComponent]:
        return Component.get_dict_changes("containers", containers, update_from)

    def update(self, service: "Service", containers: Optional[dict], update_from: Optional[dict]):
        # Note: the container set is generated when the service is started

        # Update IPAM
        ipam = service.get_ipam()

        containers_path = self.get_containers_path(service)
        container_set = self.get_container_set(service, containers)
        update_from_set = self.get_container_set(service, update_from)

        container_set.update_ipam(ipam, update_from_set)

        if ipam.is_dirty():
            ipam.save()

        # When the component is deleted, the containers config is removed
        if containers is None and os.path.isfile(containers_path):
            os.remove(containers_path)

    def get_state(self, service: "Service", containers: dict) -> State:
        container_set = self.get_container_set(service, containers)
        containers = container_set.get_containers_info()

        # If no container has been created, the service is inactive
        if citer(containers).filter(lambda c: c.get_state() != ContainerState.NOT_CREATED).first_or_default() is None:
            return ServiceState.INACTIVE

        # If at least one container is not running, the service is unhealthy
        if citer(containers).filter(lambda c: c.get_state() != ContainerState.RUNNING).first_or_default() is not None:
            return ServiceState.ACTIVE_UNHEALTHY

        # Otherwise the service is healthy
        return ServiceState.ACTIVE

    def get_status(self, service: "Service", containers: dict) -> List[TextComponent]:
        table = Table()
        container_set = self.get_container_set(service, containers)
        containers = container_set.get_containers_info()
        ipam = service.get_ipam()

        for container in containers:
            state = container.get_state()
            address = ipam.get_address_for(container.get_name())
            table.append_row(TextComponent.concat(TextComponent(state.get_color(), "● "), container.get_name()),
                             TextComponent.concat("(", TextComponent(TextColors.DARK_MAGENTA, address.get_address()), ")"),
                             f"- {container.get_status()}")

        return table.as_lines()

    def start(self, service: "Service", containers: dict):
        container_set = self.get_container_set(service, containers)
        container_set.setup()
        container_set.create(daemon=True)

    def stop(self, service: "Service", containers: dict):
        container_set = self.get_container_set(service, containers)
        if container_set.exists():
            container_set.destroy()

    def get_container_set(self, service: "Service", containers: dict) -> ContainerSet:
        containers_path = self.get_containers_path(service)
        return ContainerSet(service, ContainerSet.MAIN_SET_NAME, containers_path, containers,
                            restart_mode=RestartMode.UNLESS_STOPPED)

    def get_container_sets(self, service: "Service", containers: dict) -> List[ContainerSet]:
        return [self.get_container_set(service, containers)]

    def get_volumes(self, service: "Service", containers: dict) -> Dict[str, Volume]:
        container_set = self.get_container_set(service, containers)
        return container_set.get_volumes()
