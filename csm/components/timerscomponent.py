import copy
import os
from typing import List, Optional, TYPE_CHECKING, Dict

from pursuitlib.iterators import citer

from csm import utils
from csm.components.component import Component
from csm.container.containerset import ContainerSet
from csm.container.restartmode import RestartMode
from csm.service.serviceipam import ServiceIPAM
from csm.states.servicestate import ServiceState
from csm.states.state import State
from csm.systemdunit import SystemdUnit
from csm.volume.volume import Volume
from pursuitlib.console.table import Table
from pursuitlib.console.textcolors import TextColors
from pursuitlib.console.textcomponent import TextComponent

if TYPE_CHECKING:
    from csm.service.service import Service


class TimersComponent(Component):
    TIMERS_DIR = "timers"
    SYSTEMD_UNIT_DIR = "/etc/systemd/system"
    SERVICE_EXTENSION = ".service"
    TIMER_EXTENSION = ".timer"
    CONTAINER_PROPERTY = "container"
    AT_PROPERTY = "at"
    CMD_PROPERTY = "command"

    def __init__(self):
        super().__init__("timers")

    def list_changes(self, service: "Service", timers: Optional[dict], update_from: Optional[dict]) -> List[TextComponent]:
        created = []
        modified = []
        deleted = []
        existing = self.get_existing_timers(service)

        if timers is not None:
            for name, timer in timers.items():
                if name not in existing:
                    created.append(name)
                elif name not in update_from or timer != update_from[name]:
                    modified.append(name)
            for timer in existing:
                if timer not in timers:
                    deleted.append(timer)
        else: deleted += existing

        return Component.get_changes_lines("timers", created, modified, deleted)

    def update(self, service: "Service", timers: Optional[dict], update_from: Optional[dict]):
        existing = self.get_existing_timers(service)
        ipam = service.get_ipam()

        if timers is not None:
            for name, timer in timers.items():
                self.create_timer(service, ipam, name, timer)
            for timer in existing:
                if timer not in timers:
                    self.delete_timer(service, ipam, timer)
        elif update_from is not None:
            for timer in update_from:
                self.delete_timer(service, ipam, timer)

        SystemdUnit.reload_daemons()
        if ipam.is_dirty():
            ipam.save()

    def get_state(self, service: "Service", timers: dict) -> State:
        units = self.get_units(service)
        states = [self.get_timer_state(service, units, name, timer) for name, timer in timers.items()]
        return ServiceState.combine(*states)

    def get_status(self, service: "Service", timers: dict) -> List[TextComponent]:
        table = Table()
        units = self.get_units(service)
        ipam = service.get_ipam()

        for name, timer in timers.items():
            state = self.get_timer_state(service, units, name, timer)
            if self.has_dedicated_container(timer):
                address = ipam.get_address_for(name)
                second_col = TextComponent.concat("(", TextComponent(TextColors.DARK_MAGENTA, address.get_address()), ")")
            else:
                second_col = TextComponent.concat()

            table.append_row(TextComponent.concat(TextComponent(state.get_color(), "● "), name),
                             second_col,
                             f"- {state.get_name()}")

        return table.as_lines()

    def start(self, service: "Service", timers: dict):
        # Generate container sets
        for name, timer in timers.items():
            container_set = self.get_container_set(service, name, timer)
            if container_set is not None:
                container_set.setup()

        # Enable & start timers
        for unit in self.get_units(service):
            if not unit.get_name().endswith(TimersComponent.TIMER_EXTENSION):
                continue
            unit.enable()
            unit.start()

    def stop(self, service: "Service", timers: dict):
        # Stop & disable units
        for unit in self.get_units(service):
            unit.stop()
            unit.disable()

        # Destroy container sets
        for name, timer in timers.items():
            container_set = self.get_container_set(service, name, timer)
            if container_set is not None and container_set.exists():
                container_set.destroy()

    def get_timer_state(self, service: "Service", units: List[SystemdUnit], name: str, timer: dict) -> State:
        prefix = self.get_unit_prefix(service)
        service_unit = citer(units).filter(lambda u: u.get_name() == f"{prefix}{name}{TimersComponent.SERVICE_EXTENSION}").first_or_default()
        timer_unit = citer(units).filter(lambda u: u.get_name() == f"{prefix}{name}{TimersComponent.TIMER_EXTENSION}").first_or_default()

        active = False
        healthy = True

        # Check if units are present
        for unit in [service_unit, timer_unit]:
            if unit is None:
                healthy = False
                continue

        if timer_unit is not None:
            timer_active = timer_unit.is_active()
            if timer_active:
                active = True
            if timer_unit.is_enabled() != timer_active:
                healthy = False

        # If applicable, check if the containers are present
        if active:
            container_set = self.get_container_set(service, name, timer)
            if container_set is not None and not container_set.exists():
                healthy = False

        return ServiceState.get(active, healthy)

    def get_container_set(self, service: "Service", name: str, timer: dict) -> Optional[ContainerSet]:
        self.ensure_valid(timer)
        if not self.has_dedicated_container(timer):
            return None

        # Create container spec without timer-specified properties
        container_spec = copy.deepcopy(timer)
        del container_spec[TimersComponent.AT_PROPERTY]

        # Create container set with a single container
        container_path = self.get_timer_container_path(service, name)
        container_set = ContainerSet(service, f"timer_{name}", container_path, {name: container_spec}, restart_mode=RestartMode.NO)
        return container_set

    def get_container_sets(self, service: "Service", timers: dict) -> List[ContainerSet]:
        container_sets = []
        for name, timer in timers.items():
            container_set = self.get_container_set(service, name, timer)
            if container_set is not None:
                container_sets.append(container_set)
        return container_sets

    def get_volumes(self, service: "Service", timers: dict) -> Dict[str, Volume]:
        volumes = {}
        for container_set in self.get_container_sets(service, timers):
            for name, volume in container_set.get_volumes().items():
                volumes[name] = volume
        return volumes

    def get_existing_timers(self, service: "Service") -> List[str]:
        timers = []
        prefix = self.get_unit_prefix(service)
        for unit in self.get_units(service):
            unit_name = unit.get_name()
            if not unit_name.startswith(prefix):
                continue
            if unit_name.endswith(TimersComponent.SERVICE_EXTENSION):
                name = unit_name[len(prefix):-len(TimersComponent.SERVICE_EXTENSION)]
            elif unit_name.endswith(TimersComponent.TIMER_EXTENSION):
                name = unit_name[len(prefix):-len(TimersComponent.TIMER_EXTENSION)]
            else: continue  # If the unit is not a service nor a timer, ignore it
            if name not in timers:
                timers.append(name)
        return timers

    def get_units(self, service: "Service") -> List[SystemdUnit]:
        units = []
        prefix = self.get_unit_prefix(service)
        for item in os.listdir(TimersComponent.SYSTEMD_UNIT_DIR):
            if item.startswith(prefix):
                units.append(SystemdUnit(item))
        return units

    def create_timer(self, service: "Service", ipam: ServiceIPAM, name: str, timer: dict):
        from csm.components.components import Components

        self.ensure_valid(timer)
        prefix = self.get_unit_prefix(service)
        at = timer[TimersComponent.AT_PROPERTY]
        if self.has_dedicated_container(timer):
            container_set = self.get_container_set(service, name, timer)
            command = container_set.get_compose_cmd("up")
            # Ensure containers have assigned addresses
            ipam.get_address_for(name)
        else:
            container_name = timer[TimersComponent.CONTAINER_PROPERTY]
            timer_command = timer[TimersComponent.CMD_PROPERTY]
            containers = service.get_component_data(Components.CONTAINERS)
            if containers is None:
                raise ValueError(f"Cannot create timer ({name}) associated with container '{container_name}': no permanent containers")
            container_set = Components.CONTAINERS.get_container_set(service, containers)
            if container_name not in container_set.get_containers():
                raise ValueError(f"Cannot create timer ({name}) associated with container '{container_name}': container not found")
            command = container_set.get_compose_cmd("exec", container_name, *timer_command)
            # Ensure no obsolete IP is allocated to the timer
            ipam.free_address(name)

        # Create service
        service_path = os.path.join(TimersComponent.SYSTEMD_UNIT_DIR, f"{prefix}{name}{TimersComponent.SERVICE_EXTENSION}")
        service_spec = {
            "Unit": {
                "Description": f"Command executed by the timer '{name}' from the CSM service '{service}'",
                "StartLimitIntervalSec": 0,
            },
            "Service": {
                "Type": "simple",
                "ExecStart": utils.command_to_string(*command),
            }
        }
        self.create_unit(service_path, service_spec)

        # Create timer
        timer_path = os.path.join(TimersComponent.SYSTEMD_UNIT_DIR, f"{prefix}{name}{TimersComponent.TIMER_EXTENSION}")
        timer_spec = {
            "Unit": {
                "Description": f"Timer '{name}' from the CSM service '{service}'",
            },
            "Timer": {
                "OnCalendar": at,
                "Persistent": "true"
            },
            "Install": {
                "WantedBy": "timers.target"
            }
        }
        self.create_unit(timer_path, timer_spec)

    def delete_timer(self, service: "Service", ipam: ServiceIPAM, name: str):
        prefix = self.get_unit_prefix(service)

        service_path = os.path.join(TimersComponent.SYSTEMD_UNIT_DIR, f"{prefix}{name}{TimersComponent.SERVICE_EXTENSION}")
        if os.path.isfile(service_path):
            os.remove(service_path)
        timer_path = os.path.join(TimersComponent.SYSTEMD_UNIT_DIR, f"{prefix}{name}{TimersComponent.TIMER_EXTENSION}")
        if os.path.isfile(timer_path):
            os.remove(timer_path)

        container_path = self.get_timer_container_path(service, name)
        if os.path.isfile(container_path):
            os.remove(container_path)

        # Ensure no obsolete IP is allocated to the timer
        ipam.free_address(name)

    def create_unit(self, path: str, spec: dict):
        config = utils.create_ini_parser()
        for section, data in spec.items():
            config.add_section(section)
            config_section = config[section]
            for key, value in data.items():
                config_section[key] = str(value)
        with open(path, "w") as unit_file:
            config.write(unit_file)

    def ensure_valid(self, timer: dict):
        required = [TimersComponent.AT_PROPERTY]
        if not self.has_dedicated_container(timer):
            required.append(TimersComponent.CONTAINER_PROPERTY)
            required.append(TimersComponent.CMD_PROPERTY)

        for prop in required:
            if prop not in timer:
                raise ValueError(f"Missing required timer property: {prop}")

    def has_dedicated_container(self, timer: dict):
        return TimersComponent.CONTAINER_PROPERTY not in timer

    def get_unit_prefix(self, service: "Service") -> str:
        return f"csm_{service.get_instance_id()}_"

    def get_timer_container_path(self, service: "Service", timer: str):
        return os.path.join(service.get_data_directory(), TimersComponent.TIMERS_DIR, f"{timer}.yml")
