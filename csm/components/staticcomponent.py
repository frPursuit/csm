from typing import TYPE_CHECKING

from csm.components.component import Component
from csm.states.servicestate import ServiceState
from csm.states.state import State
from pursuitlib import errors

if TYPE_CHECKING:
    from csm.service.service import Service


# A static component that cannot be started / stopped
class StaticComponent(Component):
    def is_healthy(self, service: "Service", data: any) -> bool:
        raise errors.not_implemented(self, self.is_healthy)

    def get_state(self, service: "Service", data: any) -> State:
        return ServiceState.STATIC if self.is_healthy(service, data) else ServiceState.STATIC_UNHEALTHY

    def start(self, service: "Service", data: any):
        pass

    def stop(self, service: "Service", data: any):
        pass
