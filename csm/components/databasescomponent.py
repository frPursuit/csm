import os
from typing import Optional, List, TYPE_CHECKING, Dict

from pursuitlib.iterators import citer

from csm.components.staticcomponent import StaticComponent
from csm.config import Config
from csm.databases.databaseprovider import DatabaseProvider
from csm.registry import Registry
from csm.states.state import State
from csm.states.databasestate import DatabaseState
from pursuitlib.console.table import Table
from pursuitlib.console.textcolors import TextColors
from pursuitlib.console.textcomponent import TextComponent

if TYPE_CHECKING:
    from csm.service.service import Service


class DatabasesComponent(StaticComponent):
    def __init__(self):
        super().__init__("databases")

    def list_changes(self, service: "Service", data: Optional[dict], update_from: Optional[dict]) -> List[TextComponent]:
        created, deleted = self.get_db_changes(service, data, update_from)

        changes = []
        if len(created) > 0:
            changes.append(TextComponent(TextColors.LIGHT_GREEN, "The following databases will be created: " + ", ".join(created)))
        if len(deleted) > 0:
            changes.append(TextComponent(TextColors.LIGHT_RED, "The following databases will be deleted: " + ", ".join(deleted)))

        return changes

    def update(self, service: "Service", databases: Optional[dict], update_from: Optional[dict]):
        created, deleted = self.get_db_changes(service, databases, update_from)

        if len(created) > 0 and databases is None:
            raise ValueError("Cannot create new databases while uninstalling component")
        if len(deleted) > 0 and update_from is None:
            raise ValueError("Cannot deleted existing databases while installing component")

        providers = {}

        for name in deleted:
            db_type = update_from[name]
            provider = self.get_provider(db_type)
            if db_type not in providers:
                providers[db_type] = provider
            provider.delete_database(service, name)
        for name in created:
            db_type = databases[name]
            provider = self.get_provider(db_type)
            if db_type not in providers:
                providers[db_type] = provider
            provider.create_database(service, name)

        # If the component is uninstalled, cleanup the database providers
        if databases is None:
            for provider in providers.values():
                provider.cleanup(service)

    def is_healthy(self, service: "Service", databases: dict) -> bool:
        return citer(self.get_db_states(service, databases).values())\
            .filter(lambda s: s == DatabaseState.MISSING).first_or_default() is None

    def get_status(self, service: "Service", databases: dict) -> List[TextComponent]:
        states = self.get_db_states(service, databases)
        table = Table()

        for name, state in states.items():
            table.append_row(TextComponent.concat(TextComponent(state.get_color(), "● "), name),
                             f"- {state}")

        return table.as_lines()

    def get_context_vars(self, service: "Service", databases: dict) -> Optional[dict]:
        context = {}

        providers = {}
        for name, db_type in databases.items():
            if db_type not in providers:
                providers[db_type] = self.get_provider(db_type)

        for name, db_type in databases.items():
            provider = providers[db_type]
            context[name] = provider.get_context_vars(service, name)

        return context

    def export_data(self, service: "Service", databases: dict, target_dir: str):
        os.mkdir(target_dir)

        providers = {}
        for name, db_type in databases.items():
            if db_type not in providers:
                providers[db_type] = self.get_provider(db_type)
            target = os.path.join(target_dir, f"{name}.db")
            providers[db_type].export_database(service, name, target)

    def import_data(self, service: "Service", databases: dict, source_dir: str):
        providers = {}
        for name, db_type in databases.items():
            if db_type not in providers:
                providers[db_type] = self.get_provider(db_type)
            source = os.path.join(source_dir, f"{name}.db")
            if os.path.isfile(source):
                providers[db_type].import_database(service, name, source)

    def get_db_changes(self, service: "Service", databases: Optional[dict], update_from: Optional[dict]) -> (List[str], List[str]):
        providers = {}
        expect_exist = {}
        expect_gone = {}

        if databases is not None:
            for name, db_type in databases.items():
                if db_type not in providers:
                    providers[db_type] = self.get_provider(db_type)
                if db_type not in expect_exist:
                    expect_exist[db_type] = []
                expect_exist[db_type].append(name)

        if update_from is not None:
            for name, db_type in update_from.items():
                if db_type not in providers:
                    providers[db_type] = self.get_provider(db_type)
                if databases is not None and name in databases:
                    continue  # If the database is still used, don't delete it
                if db_type not in expect_gone:
                    expect_gone[db_type] = []
                expect_gone[db_type].append(name)

        created = []
        deleted = []

        for name, provider in providers.items():
            all_names = expect_exist.get(name, []) + expect_gone.get(name, [])
            existing = provider.get_databases(service, *all_names)

            # Compare the expected diff to the existing databases
            for db in expect_exist.get(name, []):
                if db not in existing:
                    created.append(db)
            for db in expect_gone.get(name, []):
                if db in existing:
                    deleted.append(db)

        return created, deleted

    def get_db_states(self, service: "Service", databases: dict) -> Dict[str, State]:
        providers = {}
        by_provider = {}
        existing = []

        for name, db_type in databases.items():
            if db_type not in providers:
                providers[db_type] = self.get_provider(db_type)
            if db_type not in by_provider:
                by_provider[db_type] = []
            by_provider[db_type].append(name)

        for name, provider in providers.items():
            db_list = by_provider[name]
            existing += provider.get_databases(service, *db_list)

        states = {}
        for name in databases.keys():
            states[name] = DatabaseState.AVAILABLE if name in existing else DatabaseState.MISSING
        return states

    def get_provider(self, db_type: str) -> DatabaseProvider:
        provider_type = Config.get_db_provider_type(db_type)
        return Registry.get_database_provider(db_type, provider_type)
