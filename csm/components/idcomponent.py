from csm.components.metacomponent import MetaComponent


class IdComponent(MetaComponent):
    def __init__(self):
        super().__init__("id", True)
