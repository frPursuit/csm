import os
from typing import Optional, List, TYPE_CHECKING, Dict, Tuple

import requests
from pursuitlib.console.textcolors import TextColors

from csm.components.component import Component
from csm.components.staticcomponent import StaticComponent
from csm.container.containerpath import ContainerPath
from csm.volume.volume import Volume
from csm.volume.volumetypes import VolumeTypes
from pursuitlib.console.textcomponent import TextComponent

if TYPE_CHECKING:
    from csm.service.service import Service


class ConfigVolumesComponent(StaticComponent):
    PATH_PROPERTY = "path"
    FILES_PROPERTY = "files"
    DEFAULT_VOLUME_OPTIONS = "z,ro"

    def __init__(self):
        super().__init__("config_volumes")

    def get_display_name(self) -> str:
        return "Config volumes"

    def is_healthy(self, service: "Service", volumes: dict) -> bool:
        return True

    def list_changes(self, service: "Service", volumes: Optional[dict], update_from: Optional[dict]) -> List[TextComponent]:
        volumes_changes = Component.get_dict_changes("configuration volumes", volumes, update_from)
        if volumes is not None:
            return volumes_changes + self.get_file_changes(service, volumes)
        else: return volumes_changes

    def get_file_changes(self, service: "Service", volumes: dict) -> List[TextComponent]:
        missing = self.get_missing_files(service, volumes)
        if len(missing) == 0:
            return []

        changes = [TextComponent(TextColors.LIGHT_GREEN, "The following configuration files will be downloaded:")]
        for path, url in missing:
            changes.append(TextComponent(TextColors.LIGHT_GREEN, f"  - {path}"))
        return changes

    def update(self, service: "Service", volumes: Optional[dict], update_from: Optional[dict]):
        if volumes is None:
            return

        # Create volume directories
        for name, _ in volumes.items():
            volume_root = VolumeTypes.CONFIG.get_volume_root(service, name)
            os.makedirs(volume_root, exist_ok=True)

        # Download missing config files
        missing_files = self.get_missing_files(service, volumes)
        for path, url in missing_files:
            if not os.path.isfile(path):
                parent_dir = os.path.dirname(path)
                os.makedirs(parent_dir, exist_ok=True)
                resp = requests.get(url, allow_redirects=True)
                with open(path, "wb") as file:
                    file.write(resp.content)

    def get_volumes(self, service: "Service", volumes: dict) -> Dict[str, Volume]:
        vol = {}
        for name, volume in volumes.items():
            self.ensure_valid(volume)
            container_path = ContainerPath.parse(volume[ConfigVolumesComponent.PATH_PROPERTY])
            if not container_path.is_relative_to_container():
                raise ValueError(f"Invalid path for configuration volume (not relative to container): {container_path}")
            options = container_path.get_metadata() or ConfigVolumesComponent.DEFAULT_VOLUME_OPTIONS
            vol[name] = Volume(name, service, VolumeTypes.CONFIG, container_path, options)
        return vol

    def ensure_valid(self, volume: dict):
        if ConfigVolumesComponent.PATH_PROPERTY not in volume:
            raise ValueError(f"Missing required config volume property: {ConfigVolumesComponent.PATH_PROPERTY}")

    def get_missing_files(self, service: "Service", volumes: dict) -> List[Tuple[str, str]]:
        missing = []
        for name, volume in volumes.items():
            # Ignore volumes that do not have a "files" property
            if ConfigVolumesComponent.FILES_PROPERTY not in volume:
                continue

            volume_path = VolumeTypes.CONFIG.get_volume_root(service, name)
            for path, url in volume[ConfigVolumesComponent.FILES_PROPERTY].items():
                full_path = os.path.join(volume_path, path)
                if not os.path.isfile(full_path):
                    missing.append((full_path, url))
        return missing
