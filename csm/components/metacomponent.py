from typing import TYPE_CHECKING, Optional, List

from csm.components.staticcomponent import StaticComponent
from pursuitlib.console.textcomponent import TextComponent

if TYPE_CHECKING:
    from csm.service.service import Service


# A component that only serves as metadata
class MetaComponent(StaticComponent):
    def is_healthy(self, service: "Service", data: any) -> bool:
        return True

    def list_changes(self, service: "Service", data: Optional, update_from: Optional) -> List[TextComponent]:
        return []

    def update(self, service: "Service", data: Optional, update_from: Optional):
        pass
