from csm.components.configcomponent import ConfigComponent
from csm.components.configvolumescomponent import ConfigVolumesComponent
from csm.components.containerscomponent import ContainersComponent
from csm.components.databasescomponent import DatabasesComponent
from csm.components.httpcomponent import HttpComponent
from csm.components.idcomponent import IdComponent
from csm.components.secretscomponent import SecretsComponent
from csm.components.timerscomponent import TimersComponent


class Components:
    ID = IdComponent()
    CONTAINERS = ContainersComponent()
    TIMERS = TimersComponent()
    DATABASES = DatabasesComponent()
    HTTP = HttpComponent()
    SECRETS = SecretsComponent()
    CONFIG = ConfigComponent()
    CONFIG_VOLUMES = ConfigVolumesComponent()
