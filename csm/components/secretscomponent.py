from typing import Optional, List, TYPE_CHECKING

from csm.components.staticcomponent import StaticComponent
from pursuitlib.console.textcolors import TextColors
from pursuitlib.console.textcomponent import TextComponent

if TYPE_CHECKING:
    from csm.service.service import Service


class SecretsComponent(StaticComponent):
    def __init__(self):
        super().__init__("secrets")

    def is_healthy(self, service: "Service", secrets: dict) -> bool:
        return True

    def list_changes(self, service: "Service", secrets: Optional[dict], update_from: Optional[dict]) -> List[TextComponent]:
        added = False
        deleted = False
        service_secrets = service.get_secrets()

        if secrets is not None:
            for name in secrets:
                if not service_secrets.exists(name):
                    added = True

            if update_from is not None:
                for name in update_from:
                    if name not in secrets and service_secrets.exists(name):
                        deleted = True
        elif not service_secrets.is_empty():
            deleted = True

        changes = []
        if added:
            changes.append(TextComponent(TextColors.LIGHT_GREEN, "Secrets will be added"))
        if deleted:
            changes.append(TextComponent(TextColors.LIGHT_RED, "Secrets will be deleted"))
        return changes

    def update(self, service: "Service", secrets: Optional[dict], update_from: Optional[dict]):
        if secrets is None:
            return

        service_secrets = service.get_secrets()

        # Ensure all secrets are generated
        for name, length in secrets.items():
            service_secrets.get_secret(name, length)

        # Ensure old secrets are removed
        if update_from is not None:
            for name in update_from:
                if name not in secrets:
                    service_secrets.remove_secret(name)

        if service_secrets.is_dirty():
            service_secrets.save()
