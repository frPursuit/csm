import os.path
from configparser import SectionProxy
from typing import Optional, List, TYPE_CHECKING, Dict

from jinja2 import Template
from pursuitlib.utils import is_null_or_empty

from csm.certbot import Certbot
from csm.components.staticcomponent import StaticComponent
from csm.container.containerpath import ContainerPath
from csm.states.state import State
from csm.states.systemdstate import SystemdState
from csm.systemdunit import SystemdUnit
from csm.volume.volume import Volume
from csm.volume.volumetypes import VolumeTypes
from pursuitlib.console.textcolors import TextColors
from pursuitlib.console.textcomponent import TextComponent

if TYPE_CHECKING:
    from csm.service.service import Service


class HttpComponent(StaticComponent):
    NGINX_DIRECTORY = "/etc/nginx/conf.d"
    NGINX = SystemdUnit("nginx.service")
    NO_DOMAIN_STATE = State("domain_not_configured", TextColors.DARK_RED, "Domain not configured")

    def __init__(self):
        super().__init__("http")

    def get_display_name(self) -> str:
        return "HTTP"

    def list_changes(self, service: "Service", endpoints: Optional[dict], update_from: Optional[dict]) -> List[TextComponent]:
        if endpoints is not None:
            if update_from is None:
                return [TextComponent(TextColors.LIGHT_GREEN, "The HTTP configuration will be created")]
            elif endpoints != update_from:
                return [TextComponent(TextColors.LIGHT_BLUE, "The HTTP configuration will be updated")]
        elif update_from is not None:
            return [TextComponent(TextColors.LIGHT_RED, "The HTTP configuration will be deleted")]

        return []

    def update(self, service: "Service", endpoints: Optional[dict], update_from: Optional[dict]):
        # The HTTP configuration is generated when the service is started

        # Remove the nginx configuration when the component is removed
        if endpoints is None:
            nginx_config = self.get_nginx_config_path(service)
            if os.path.isfile(nginx_config):
                os.remove(nginx_config)
                HttpComponent.NGINX.restart()

        # If a certificate is used, delete it
        config = self.get_config(service)
        tls = config.get("tls", "false").lower() == "true"
        domain = config.get("domain", "")
        if tls and not is_null_or_empty(domain):
            Certbot.delete_certificate(domain)

    def get_default_config(self, service: "Service", endpoints: dict) -> Optional[dict]:
        return {
            "domain": "",
            "tls": False
        }

    def is_healthy(self, service: "Service", endpoints: dict) -> bool:
        return SystemdState.is_active(self.get_http_state(service))

    def get_status(self, service: "Service", endpoints: dict) -> List[TextComponent]:
        http_state = self.get_http_state(service)
        # Use the LIGHT_RED color if the HTTP service is not active
        state_color = http_state.get_color() if SystemdState.is_active(http_state) else TextColors.LIGHT_RED
        return [TextComponent.concat(TextComponent(state_color, "● "), http_state.get_name())]

    def get_http_state(self, service: "Service") -> State:
        config = self.get_config(service)
        domain = config.get("domain", "")
        if is_null_or_empty(domain):
            return HttpComponent.NO_DOMAIN_STATE

        nginx_config = self.get_nginx_config_path(service)
        if not os.path.isfile(nginx_config):
            return SystemdState.MISSING

        return HttpComponent.NGINX.get_state()

    def start(self, service: "Service", endpoints: dict):
        config = self.get_config(service)

        # Generate a TLS certificate if necessary
        tls = config.get("tls", "false").lower() == "true"
        domain = config.get("domain", "")
        if tls and not is_null_or_empty(domain) and not Certbot.certificate_exists(domain):
            print("Generating TLS certificate...")
            Certbot.generate_certificate(domain)

        intended_nginx_config = self.get_intended_nginx_config(service, config, endpoints)

        nginx_config = self.get_nginx_config_path(service)
        if os.path.isfile(nginx_config):
            with open(nginx_config, "r") as nginx_config_file:
                current_nginx_config = nginx_config_file.read()
        else: current_nginx_config = None

        # Update the nginx configuration if necessary
        if current_nginx_config != intended_nginx_config:
            self.update_nginx_config(service, intended_nginx_config)

    def get_context_vars(self, service: "Service", endpoints: dict) -> Optional[dict]:
        return {}

    def get_volumes(self, service: "Service", endpoints: dict) -> Dict[str, Volume]:
        volumes = {}
        for endpoint, content in endpoints.items():
            path = ContainerPath.parse(content)
            # Create a volume for each exposed directory inside the container
            if not path.is_tcp_port() and path.is_relative_to_container():
                name = self.get_volume_name(endpoint)
                volumes[name] = Volume(name, service, VolumeTypes.VOLATILE, path, path.get_metadata())
        return volumes

    def get_volume_name(self, endpoint: str):
        return "http" + endpoint.replace('/', '_')

    def get_intended_nginx_config(self, service: "Service", config: SectionProxy, endpoints: dict) -> Optional[str]:
        volumes = service.get_volumes()
        domain = config.get("domain", "")
        tls = config.get("tls", "false").lower() == "true"
        upload_limit = config.get("upload_limit", "1M")

        if is_null_or_empty(domain):
            return None  # If the domain isn't set, the nginx config cannot be generated

        endpoint_list = []
        for endpoint, content in endpoints.items():
            path = ContainerPath.parse(content)
            if path.is_tcp_port():
                container = path.get_relative_to()
                proxy_domain = service.get_domain(container)
                proxy_port = int(path.path)
                endpoint_list.append({"path": endpoint, "proxy": f"http://{proxy_domain}:{proxy_port}"})
            elif path.is_relative_to_container():
                # Because a path cannot be directly exposed inside the container, a
                # volatile volume has been created for the exposed directory
                volume_name = self.get_volume_name(endpoint)
                volume_path = ContainerPath.relative_to_volume(volume_name, "/")
                serve = service.get_host_path(volume_path, volumes)
                endpoint_list.append({"path": endpoint, "serve": serve})
            elif path.is_relative_to_volume():
                serve = service.get_host_path(path, volumes)
                endpoint_list.append({"path": endpoint, "serve": serve})

        context = {
            "tls": tls,
            "domain": domain,
            "upload_limit": upload_limit,
            "endpoints": endpoint_list
        }

        if tls:
            context["certificate_path"] = Certbot.get_certificate_path(domain)
            context["certificate_key_path"] = Certbot.get_certificate_key_path(domain)

        template_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "templates/nginx.conf.j2")
        with open(template_path, "r") as template_file:
            template = Template(template_file.read())
        return template.render(**context) + '\n'  # Finish with a newline

    def get_nginx_config_path(self, service: "Service") -> str:
        return os.path.join(HttpComponent.NGINX_DIRECTORY, f"csm_{service.get_instance_id()}.conf")

    def update_nginx_config(self, service: "Service", config: Optional[str]):
        if not os.path.isdir(HttpComponent.NGINX_DIRECTORY):
            raise IOError("Nginx configuration directory not detected")

        config_path = self.get_nginx_config_path(service)
        if config is not None:
            with open(config_path, "w") as config_file:
                config_file.write(config)
        elif os.path.isfile(config_path):
            os.remove(config_path)

        HttpComponent.NGINX.restart()
