from configparser import SectionProxy
from typing import TYPE_CHECKING, Dict, List, Optional, Tuple

from csm.container.containerset import ContainerSet
from csm.states.state import State
from csm.volume.volume import Volume
from pursuitlib import errors
from pursuitlib.console.textcolors import TextColors
from pursuitlib.console.textcomponent import TextComponent

if TYPE_CHECKING:
    from csm.service.service import Service


class Component:
    @staticmethod
    def get_changes_lines(name: str, created: list, modified: list, deleted: list) -> List[TextComponent]:
        changes = []
        if len(created) > 0:
            changes.append(TextComponent(TextColors.LIGHT_GREEN, f"The following {name} will be created: " + ", ".join(created)))
        if len(modified) > 0:
            changes.append(TextComponent(TextColors.LIGHT_BLUE, f"The following {name} will be modified: " + ", ".join(modified)))
        if len(deleted) > 0:
            changes.append(TextComponent(TextColors.LIGHT_RED, f"The following {name} will be deleted: " + ", ".join(deleted)))
        return changes

    @staticmethod
    def get_dict_diff(to_dict: Optional[dict], from_dict: Optional[dict]) -> Tuple[list, list, list]:
        created = []
        modified = []
        deleted = []

        if to_dict is not None and from_dict is not None:
            for container in to_dict.keys():
                if container not in from_dict:
                    created.append(container)
                elif container in from_dict and to_dict[container] != from_dict[container]:
                    modified.append(container)
            for container in from_dict.keys():
                if container not in to_dict:
                    deleted.append(container)
        elif to_dict is not None:
            created += to_dict.keys()
        elif from_dict is not None:
            deleted += from_dict.keys()

        return created, modified, deleted

    @staticmethod
    def get_dict_changes(name: str, to_dict: Optional[dict], from_dict: Optional[dict]) -> List[TextComponent]:
        created, modified, deleted = Component.get_dict_diff(to_dict, from_dict)
        return Component.get_changes_lines(name, created, modified, deleted)

    def __init__(self, name: str, required: bool = False):
        self.name = name
        self.required = required

    def get_name(self) -> str:
        return self.name

    def get_display_name(self) -> str:
        return self.name.capitalize()

    def is_required(self) -> bool:
        return self.required

    def list_changes(self, service: "Service", data: Optional, update_from: Optional) -> List[TextComponent]:
        raise errors.not_implemented(self, self.list_changes)

    def update(self, service: "Service", data: Optional, update_from: Optional):
        raise errors.not_implemented(self, self.update)

    def get_default_config(self, service: "Service", data: any) -> Optional[dict]:
        return None

    def get_context_vars(self, service: "Service", data: any) -> Optional[dict]:
        return None

    # Should return a value from the ServiceState class
    def get_state(self, service: "Service", data: any) -> State:
        raise errors.not_implemented(self, self.get_state)

    def get_status(self, service: "Service", data: any) -> List[TextComponent]:
        return []

    def start(self, service: "Service", data: any):
        raise errors.not_implemented(self, self.start)

    def stop(self, service: "Service", data: any):
        raise errors.not_implemented(self, self.stop)

    def get_container_sets(self, service: "Service", data: any) -> List[ContainerSet]:
        return []

    def get_volumes(self, service: "Service", data: any) -> Dict[str, Volume]:
        return {}

    def get_config(self, service: "Service") -> SectionProxy:
        return service.get_config().get_section(self.get_name())

    def export_data(self, service: "Service", data: any, target_dir: str):
        # "target_dir" has to be created by the module if necessary
        pass

    def import_data(self, service: "Service", data: any, source_dir: str):
        # "source_dir" does not necessarily exist
        pass

    def __str__(self) -> str:
        return self.get_name()
