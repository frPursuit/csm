from pursuitlib.console.textcolor import TextColor


class State:
    def __init__(self, state_id: str, color: TextColor, name: str):
        self.id = state_id
        self.color = color
        self.name = name

    def get_id(self) -> str:
        return self.id

    def get_color(self) -> TextColor:
        return self.color

    def get_name(self) -> str:
        return self.name

    def __str__(self) -> str:
        return self.id
