from pursuitlib.iterators import citer

from csm.states.state import State
from pursuitlib.console.textcolors import TextColors


class ServiceState:
    ACTIVE = State("active", TextColors.LIGHT_GREEN, "Active")
    ACTIVE_UNHEALTHY = State("unhealthy", TextColors.LIGHT_RED, "Active (Unhealthy)")
    INACTIVE = State("inactive", TextColors.LIGHT_GRAY, "Inactive")
    INACTIVE_UNHEALTHY = State("unhealthy", TextColors.DARK_YELLOW, "Inactive (Unhealthy)")
    STATIC = State("static", TextColors.LIGHT_GRAY, "Static")
    STATIC_UNHEALTHY = State("static_unhealthy", TextColors.DARK_YELLOW, "Static (Unhealthy)")

    @staticmethod
    def is_active(state: State):
        return state == ServiceState.ACTIVE or state == ServiceState.ACTIVE_UNHEALTHY

    @staticmethod
    def is_inactive(state: State):
        return state == ServiceState.INACTIVE or state == ServiceState.INACTIVE_UNHEALTHY

    @staticmethod
    def is_static(state: State):
        return state == ServiceState.STATIC or state == ServiceState.STATIC_UNHEALTHY

    @staticmethod
    def is_healthy(state: State):
        return state == ServiceState.ACTIVE or state == ServiceState.INACTIVE or state == ServiceState.STATIC

    @staticmethod
    def get(active: bool, healthy: bool):
        if active:
            return ServiceState.ACTIVE if healthy else ServiceState.ACTIVE_UNHEALTHY
        else:  # if not active
            return ServiceState.INACTIVE if healthy else ServiceState.INACTIVE_UNHEALTHY

    @staticmethod
    def combine(*states: State):
        # If at least one component is active, the service is active
        active = citer(states).filter(lambda s: ServiceState.is_active(s)).first_or_default() is not None
        no_inactive = citer(states).filter(lambda s: ServiceState.is_inactive(s)).first_or_default() is None

        # The service is healthy if:
        #   - All components are healthy
        #   - There is no inactive service while the service is active
        healthy = (active == no_inactive) and citer(states).filter(lambda s: not ServiceState.is_healthy(s)).first_or_default() is None

        return ServiceState.get(active, healthy)
