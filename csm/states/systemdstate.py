from csm.states.state import State
from pursuitlib.console.textcolors import TextColors


class SystemdState:
    ENABLED_ACTIVE = State("enabled_active", TextColors.LIGHT_GREEN, "Active")
    ENABLED_INACTIVE = State("enabled_inactive", TextColors.LIGHT_GRAY, "Inactive")
    NOT_ENABLED_ACTIVE = State("not_enabled_active", TextColors.LIGHT_YELLOW, "Active (not enabled)")
    NOT_ENABLED_INACTIVE = State("not_enabled_inactive", TextColors.DARK_YELLOW, "Inactive")
    MISSING = State("missing", TextColors.LIGHT_RED, "Missing")

    @staticmethod
    def is_enabled(state: State):
        return state == SystemdState.ENABLED_ACTIVE or state == SystemdState.ENABLED_INACTIVE

    @staticmethod
    def is_active(state: State):
        return state == SystemdState.ENABLED_ACTIVE or state == SystemdState.NOT_ENABLED_ACTIVE

    @staticmethod
    def get(enabled: bool, active: bool):
        if enabled:
            return SystemdState.ENABLED_ACTIVE if active else SystemdState.ENABLED_INACTIVE
        else:  # if not enabled
            return SystemdState.NOT_ENABLED_ACTIVE if active else SystemdState.NOT_ENABLED_INACTIVE
