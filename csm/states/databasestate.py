from csm.states.state import State
from pursuitlib.console.textcolors import TextColors


class DatabaseState:
    AVAILABLE = State("available", TextColors.LIGHT_GREEN, "Available")
    MISSING = State("missing", TextColors.LIGHT_RED, "Missing")
