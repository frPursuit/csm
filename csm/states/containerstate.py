from csm.states.state import State
from pursuitlib.console.textcolors import TextColors


class ContainerState:
    PAUSED = State("paused", TextColors.DARK_YELLOW, "Paused")
    RESTARTING = State("restarting", TextColors.LIGHT_MAGENTA, "Restarting")
    REMOVING = State("removing", TextColors.DARK_RED, "Removing")
    RUNNING = State("running", TextColors.LIGHT_GREEN, "Running")
    DEAD = State("dead", TextColors.LIGHT_RED, "Dead")
    CREATED = State("created", TextColors.LIGHT_YELLOW, "Created")
    EXITED = State("exited", TextColors.LIGHT_GRAY, "Exited")
    NOT_CREATED = State("not_created", TextColors.LIGHT_GRAY, "Not created")

    @staticmethod
    def get(state_id: str) -> State:
        for item in dir(ContainerState):
            # Ignore internal fields
            if item.startswith("_"):
                continue

            state: State = getattr(ContainerState, item)
            if state.get_id() == state_id:
                return state
        raise ValueError(f"Unknown container state: {state_id}")
