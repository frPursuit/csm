import os
from typing import Optional, TYPE_CHECKING, Dict

from csm import utils
from csm.container.containerpath import ContainerPath
from csm.utils import absolute_path_join
from csm.volume.volumetype import VolumeType

if TYPE_CHECKING:
    from csm.service.service import Service


class Volume:
    DATA_VOLUME_DIR = "data"  # This folder should be included in backups / migrations
    VOLATILE_VOLUME_DIR = "volatile"  # This folder should not be included in backups / migrations
    DEFAULT_USER = "root"
    USER_OPTION = "user"
    GROUP_OPTION = "group"
    CSM_OPTIONS = (USER_OPTION, GROUP_OPTION)

    def __init__(self, name: str, service: "Service", vtype: VolumeType, path: ContainerPath, options: Optional[str] = None):
        if not path.is_relative_to_container():
            raise ValueError(f"Invalid container path '{path}': the provided path should be relative to a container")

        self.name = name
        self.service = service
        self.type = vtype
        self.path = path

        # Parse options
        self.options = {}
        if options is not None:
            for option in options.split(','):
                if "=" in option:
                    index = option.index('=')
                    key = option[:index].strip()
                    value = option[index+1:].strip()
                else:
                    key = option.strip()
                    value = True
                self.options[key] = value

    def get_name(self) -> str:
        return self.name

    def get_service(self) -> "Service":
        return self.service

    def get_type(self) -> VolumeType:
        return self.type

    def get_container(self) -> str:
        return self.path.get_relative_to()

    def get_internal_path(self) -> str:
        return self.path.get_internal_path()

    def get_option(self, name: str) -> any:
        return self.options[name]

    def has_option(self, name: str) -> bool:
        return name in self.options

    def get_options(self) -> Dict[str, any]:
        return self.options

    def get_options_string(self, docker_only: bool = False) -> Optional[str]:
        options = []
        for key, value in self.options.items():
            if docker_only and key in Volume.CSM_OPTIONS:
                continue
            if isinstance(value, bool) and value:
                # If the value is "true", only append the key
                options.append(key)
            else:
                # Otherwise specify the value explicitly
                options.append(f"{key}={value}")

        return ','.join(options) if len(options) > 0 else None

    def get_host_path(self, relative_path: Optional[str] = None) -> str:
        volume_root = self.get_type().get_volume_root(self.get_service(), self.get_name())
        return absolute_path_join(volume_root, relative_path) if relative_path is not None else volume_root

    def get_definition(self, docker_options: bool = False) -> str:
        host_path = self.get_host_path()
        options = self.get_options_string(docker_options)

        if options is not None:
            return f"{host_path}:{self.path.path}:{options}"
        else: return f"{host_path}:{self.path.path}"

    def setup_external_dir(self):
        host_path = self.get_host_path()
        os.makedirs(host_path, exist_ok=True)

        user = self.get_option(Volume.USER_OPTION) if self.has_option(Volume.USER_OPTION) else Volume.DEFAULT_USER
        group = self.get_option(Volume.GROUP_OPTION) if self.has_option(Volume.GROUP_OPTION) else user
        utils.chown(host_path, user, group)

    def __str__(self):
        return self.get_definition()
