from csm.directories import Directories
from csm.volume.volumetype import VolumeType


class VolumeTypes:
    DATA = VolumeType(Directories.SERVICES_DATA, "data")
    VOLATILE = VolumeType(Directories.SERVICES_DATA, "volatile")
    CONFIG = VolumeType(Directories.SERVICES_CONFIG, "")
