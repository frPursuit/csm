import os.path
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from csm.service.service import Service


class VolumeType:
    def __init__(self, global_dir: str, sub_dir: str):
        self.global_dir = global_dir
        self.sub_dir = sub_dir

    def get_global_dir(self) -> str:
        return self.global_dir

    def get_sub_dir(self) -> str:
        return self.sub_dir

    def get_volume_root(self, service: "Service", volume: str):
        return os.path.join(self.global_dir, service.get_instance_id(), self.sub_dir, volume)
