from argparse import ArgumentParser
from typing import List

from csm.actions.action import Action
from csm.service.service import Service
from pursuitlib.console.table import Table
from pursuitlib.console.textcolors import TextColors
from pursuitlib.console.textcomponent import TextComponent


class ListAction(Action):
    def __init__(self):
        super().__init__("list")

    def get_description(self) -> str:
        return "List installed services"

    def get_aliases(self) -> List[str]:
        return ["ls"]

    def execute(self, args):
        services = Service.list_installed()

        if len(services) == 0:
            return

        if args.names:
            print(' '.join([service.get_instance_id() for service in services]))
        else:
            table = Table()
            for service in services:
                name = service.get_instance_id()
                state = service.get_state()
                network = service.get_ipam().get_network_address()
                table.append_row(TextComponent.concat(TextComponent(state.get_color(), "● "), name),
                                 TextComponent.concat("- ", TextComponent(TextColors.DARK_BLUE, network)))
            print(table)

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("--names", "-n", action="store_true", help="Only display the names of installed services")
        return parser
