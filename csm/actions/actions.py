from csm.actions.composeaction import ComposeAction
from csm.actions.execaction import ExecAction
from csm.actions.exportaction import ExportAction
from csm.actions.exportkeyaction import ExportKeyAction
from csm.actions.extractaction import ExtractAction
from csm.actions.importaction import ImportAction
from csm.actions.importkeyaction import ImportKeyAction
from csm.actions.installaction import InstallAction
from csm.actions.listaction import ListAction
from csm.actions.logsaction import LogsAction
from csm.actions.removeaction import RemoveAction
from csm.actions.repairaction import RepairAction
from csm.actions.restartaction import RestartAction
from csm.actions.startaction import StartAction
from csm.actions.statusaction import StatusAction
from csm.actions.stopaction import StopAction
from csm.actions.timeractive import TimerAction


class Actions:
    LIST = ListAction()
    INSTALL = InstallAction()
    START = StartAction()
    STOP = StopAction()
    RESTART = RestartAction()
    REPAIR = RepairAction()
    REMOVE = RemoveAction()
    STATUS = StatusAction()
    LOGS = LogsAction()
    EXEC = ExecAction()
    COMPOSE = ComposeAction()
    TIMER = TimerAction()
    EXPORT = ExportAction()
    IMPORT = ImportAction()
    EXTRACT = ExtractAction()
    EXPORTKEY = ExportKeyAction()
    IMPORTKEY = ImportKeyAction()
