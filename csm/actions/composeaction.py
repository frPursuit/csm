from argparse import ArgumentParser

from csm.actions.action import Action
from csm.service.service import Service


class ComposeAction(Action):
    def __init__(self):
        super().__init__("compose")

    def get_description(self) -> str:
        return "Execute a docker compose command"

    def execute(self, args):
        service = Service.from_id(args.service)
        containers = service.get_main_container_set()
        containers.execute_compose_cmd(*args.command)

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("service", help="The service to execute the compose command on")
        parser.add_argument("command", nargs="*", help="The compose command to execute")
        return parser
