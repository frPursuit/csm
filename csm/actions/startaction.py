from argparse import ArgumentParser

from csm.actions.action import Action
from csm.service.service import Service


class StartAction(Action):
    def __init__(self):
        super().__init__("start")

    def get_description(self) -> str:
        return "Start a service"

    def execute(self, args):
        instance_id = args.service
        service = Service.from_id(instance_id)
        service.start()

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("service", help="The service to start")
        return parser
