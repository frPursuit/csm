import os.path
from argparse import ArgumentParser

from csm.actions.action import Action
from csm.service.serviceimage import ServiceImage


class ExtractAction(Action):
    def __init__(self):
        super().__init__("extract")

    def get_description(self) -> str:
        return "Extract a service image"

    def execute(self, args):
        source = args.source
        target = args.target

        if not ServiceImage.key_exists():
            raise IOError("No image key exists")

        print("Extracting service image...")
        os.mkdir(target)
        ServiceImage.extract(ServiceImage.get_key(), source, target)
        print(f"Service image extracted to '{target}'")

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("source", help="The service image to extract")
        parser.add_argument("target", help="The directory to extract the image to")
        return parser
