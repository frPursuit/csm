from argparse import ArgumentParser

from csm.actions.action import Action
from csm.service.service import Service
from csm.service.serviceimage import ServiceImage


class ExportAction(Action):
    def __init__(self):
        super().__init__("export")

    def get_description(self) -> str:
        return "Export a service and its data"

    def execute(self, args):
        instance_id = args.service
        service = Service.from_id(instance_id)

        output = args.output or f"{service}.{ServiceImage.EXTENSION}"
        print(f"Exporting {service}...")
        service.export_to(output)
        print(f"Created export '{output}'")

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("service", help="The service to export")
        parser.add_argument("output", help="The file to save to output to. Default: <instance ID>.svi", nargs='?')
        return parser
