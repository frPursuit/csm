from argparse import ArgumentParser

from csm.actions.action import Action
from csm.service.serviceimage import ServiceImage
from pursuitlib.console import console
from pursuitlib.console.textcolors import TextColors


class ImportKeyAction(Action):
    def __init__(self):
        super().__init__("importkey")

    def get_description(self) -> str:
        return "Import a CSM image key"

    def execute(self, args):
        key = args.key

        if ServiceImage.key_exists():
            console.write_line("An image key already exists. Overwriting it could mean losing access to previous service exports.",
                           TextColors.DARK_YELLOW)
            if not console.confirm("Are you sure you want to overwrite it?", False):
                return

        ServiceImage.set_key(bytes.fromhex(key))

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("key", help="The hex-encoded key to import")
        return parser
