import os.path
from argparse import ArgumentParser

from csm.actions.action import Action
from csm.service.service import Service


class ImportAction(Action):
    def __init__(self):
        super().__init__("import")

    def get_description(self) -> str:
        return "Import a service and its data"

    def execute(self, args):
        source = args.source
        instance_id = args.id

        if not os.path.isfile(source):
            raise IOError(f"File not found: {source}")
        if Service.try_from_id(instance_id) is not None:
            raise ValueError(f"Service already exists: {instance_id}")

        print(f"Importing {instance_id}...")
        service = Service.import_from(source, instance_id)
        print(f"Service imported")

        if args.start:
            service.start()

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("source", help="The service image to import")
        parser.add_argument("id", help="The instance ID of the imported service")
        parser.add_argument("-s", "--start", action="store_true", help="Start the service after it has been imported")
        return parser
