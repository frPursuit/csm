from argparse import ArgumentParser

from csm import utils
from csm.actions.action import Action
from csm.components.components import Components
from csm.service.service import Service


class TimerAction(Action):
    def __init__(self):
        super().__init__("timer")

    def get_description(self) -> str:
        return "Display the status or logs of a timer"

    def execute(self, args):
        instance_id = args.service
        service = Service.from_id(instance_id)

        prefix = Components.TIMERS.get_unit_prefix(service)
        suffix = ".service" if args.element == "service" else ".timer"
        unit = f"{prefix}{args.timer}{suffix}"

        try:
            if args.show == "status":
                utils.run("systemctl", "status", unit)
            elif args.show == "logs":
                utils.run("journalctl", "-eu", unit)
        except ChildProcessError:
            pass  # Ignore nonzero return codes, which can be the result of normal user interaction

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("element", choices=["unit", "service"], help="The element to show the status/logs of")
        parser.add_argument("show", choices=["status", "logs"], help="The element to show")
        parser.add_argument("service", help="The service to display the timer of")
        parser.add_argument("timer", help="The timer to display the element of")
        return parser
