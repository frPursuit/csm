from argparse import ArgumentParser

from csm.actions.action import Action
from csm.service.service import Service


class ExecAction(Action):
    def __init__(self):
        super().__init__("exec")

    def get_description(self) -> str:
        return "Execute a command inside a container"

    def execute(self, args):
        service = Service.from_id(args.service)
        containers = service.get_main_container_set()
        containers.execute_compose_cmd("exec", args.container, *args.command)

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("service", help="The service to execute a command on")
        parser.add_argument("container", help="The container to execute a command inside of")
        parser.add_argument("command", nargs="+", help="The command to execute")
        return parser
