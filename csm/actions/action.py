from argparse import ArgumentParser
from typing import Optional, List


class Action:
    def __init__(self, name: str):
        self.name = name

    def get_name(self) -> str:
        return self.name

    def get_aliases(self) -> List[str]:
        return []

    def get_description(self) -> Optional[str]:
        return None

    def execute(self, args):
        pass

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = subparsers.add_parser(self.get_name(), aliases=self.get_aliases(), help=self.get_description())
        return parser
