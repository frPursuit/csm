from argparse import ArgumentParser

from csm.actions.action import Action
from csm.service.service import Service
from csm.states.servicestate import ServiceState
from pursuitlib.console import console
from pursuitlib.console.textcolors import TextColors
from pursuitlib.console.textcomponent import TextComponent


class RemoveAction(Action):
    def __init__(self):
        super().__init__("remove")

    def get_description(self) -> str:
        return "Remove an installed service"

    def execute(self, args):
        instance_id = args.service
        service = Service.from_id(instance_id)

        display = TextComponent()
        display.appendline(TextComponent(TextColors.LIGHT_RED, f"-------- Removing service {service}"))
        display.appendline("\nThe following changes will be made:\n")

        changes = service.list_remove_changes()
        for component, component_changes in changes:
            display.appendline(TextColors.LIGHT_YELLOW, f"---- {component.get_display_name()}")
            for change in component_changes:
                display.appendline(change)

        console.write(display)

        console.write_line()
        if args.dry_run or (not args.yes and not console.confirm("Do you want to continue?", False)):
            return

        if ServiceState.is_active(service.get_state()):
            print("Stopping service...")
            service.stop()

        service.remove()

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("service", help="The service to remove")
        parser.add_argument("-y", "--yes", action="store_true", help="Confirm the uninstallation of the service")
        parser.add_argument("--dry-run", action="store_true", help="List the changes instead of removing the service")
        return parser
