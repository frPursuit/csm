from argparse import ArgumentParser

from csm.actions.action import Action
from csm.service.service import Service
from csm.states.servicestate import ServiceState


class RepairAction(Action):
    def __init__(self):
        super().__init__("repair")

    def get_description(self) -> str:
        return "Repair an installed service"

    def execute(self, args):
        instance_id = args.service
        start = args.start
        service = Service.from_id(instance_id)

        if ServiceState.is_active(service.get_state()):
            print("Stopping service...")
            service.stop()
            start = True

        print("Repairing service...")
        service.install(service)

        if start:
            print("Starting service...")
            service.start()

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("service", help="The service to repair")
        parser.add_argument("-s", "--start", action="store_true", help="Start the service after it has been repaired")
        return parser
