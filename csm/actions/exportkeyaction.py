from csm.actions.action import Action
from csm.service.serviceimage import ServiceImage


class ExportKeyAction(Action):
    def __init__(self):
        super().__init__("exportkey")

    def get_description(self) -> str:
        return "Export the CSM image key"

    def execute(self, args):
        if not ServiceImage.key_exists():
            raise IOError("No CSM image key to export")
        print(ServiceImage.get_key().hex())
