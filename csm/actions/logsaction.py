from argparse import ArgumentParser

from csm.actions.action import Action
from csm.service.service import Service


class LogsAction(Action):
    def __init__(self):
        super().__init__("logs")

    def get_description(self) -> str:
        return "Display the logs of a service"

    def execute(self, args):
        instance_id = args.service
        service = Service.from_id(instance_id)
        containers = service.get_main_container_set()

        if args.container is not None:
            containers.execute_compose_cmd("logs", args.container)
        else: containers.execute_compose_cmd("logs")

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("service", help="The service to display the logs of")
        parser.add_argument("container", nargs='?', help="The container to display the logs of")
        return parser
