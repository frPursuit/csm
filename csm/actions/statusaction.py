from argparse import ArgumentParser

from csm.actions.action import Action
from csm.registry import Registry
from csm.service.service import Service
from pursuitlib.console import console
from pursuitlib.console.textcolors import TextColors
from pursuitlib.console.textcomponent import TextComponent


class StatusAction(Action):
    FIRST_ROW_WIDTH = 11

    @staticmethod
    def get_title(title: str):
        if len(title) > StatusAction.FIRST_ROW_WIDTH:
            return title
        padding = ' ' * (StatusAction.FIRST_ROW_WIDTH - len(title))
        return f"{padding}{title}"

    def __init__(self):
        super().__init__("status")

    def get_description(self) -> str:
        return "Check the status of a service"

    def execute(self, args):
        instance_id = args.service
        service = Service.from_id(instance_id)
        state = service.get_state()
        ipam = service.get_ipam()

        display = TextComponent()
        display.appendline(TextComponent(state.get_color(), "● "), instance_id)
        display.appendline(StatusAction.get_title("Loaded"), ": ", service.get_spec_path())
        display.appendline(StatusAction.get_title("Status"), ": ", TextComponent(state.get_color(), state.get_name()))
        display.appendline(StatusAction.get_title("Network"), ": ", TextComponent(TextColors.DARK_BLUE, ipam.get_network_address()))

        for component in Registry.get_components():
            data = service.get_component_data(component)
            if data is None:
                continue
            status = component.get_status(service, data)
            if len(status) == 0:
                continue
            display.appendline(StatusAction.get_title(component.get_display_name()), ": ", status[0])
            for line in status[1:]:
                display.appendline(' ' * (StatusAction.FIRST_ROW_WIDTH + 2), line)

        console.write(display)

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("service", help="The service to check the status of")
        return parser
