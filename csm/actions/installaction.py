from argparse import ArgumentParser
from typing import Optional

from csm.actions.action import Action
from csm.service.service import Service
from csm.states.servicestate import ServiceState
from pursuitlib.console import console
from pursuitlib.console.textcolors import TextColors
from pursuitlib.console.textcomponent import TextComponent


class InstallAction(Action):
    def __init__(self):
        super().__init__("install")

    def get_description(self) -> str:
        return "Install a service from a service specification"

    def execute(self, args):
        instance_id: Optional[str] = args.id
        spec_path: str = args.spec
        start = args.start

        spec_low = spec_path.lower()
        if spec_low.startswith("http://") or spec_low.startswith("https://"):
            service = Service.from_url(spec_path, instance_id)
        else: service = Service.from_file(spec_path, instance_id)

        existing = Service.try_from_id(service.get_instance_id())
        changes = service.list_changes(existing)

        if len(changes) > 0:
            verb = "Installing" if existing is None else "Updating"
            color = TextColors.LIGHT_GREEN if existing is None else TextColors.LIGHT_BLUE
            display = TextComponent()
            display.appendline(TextComponent(color, f"-------- {verb} service {service}"))
            display.appendline("\nThe following changes will be made:\n")

            changes = service.list_changes(existing)
            for component, component_changes in changes:
                display.appendline(TextColors.LIGHT_YELLOW, f"---- {component.get_display_name()}")
                for change in component_changes:
                    display.appendline(change)

            console.write(display)

            console.write_line()
            if args.dry_run or (not args.yes and not console.confirm("Do you want to continue?", existing is None)):
                return

            if existing is not None and ServiceState.is_active(existing.get_state()):
                print("Stopping service...")
                existing.stop()
                start = True

            service.install(existing)
        else: print("This version of the service is already installed: nothing to do")

        if start:
            print("Starting service...")
            service.start()

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("spec")
        parser.add_argument("-i", "--id", required=False, help="The instance ID of the service. Defaults to the service ID")
        parser.add_argument("-y", "--yes", action="store_true", help="Confirm the installation of the service")
        parser.add_argument("-s", "--start", action="store_true", help="Start the service after it has been installed")
        parser.add_argument("--dry-run", action="store_true", help="List the changes instead of installing the service")
        return parser
