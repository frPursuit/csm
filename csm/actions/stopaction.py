from argparse import ArgumentParser

from csm.actions.action import Action
from csm.service.service import Service


class StopAction(Action):
    def __init__(self):
        super().__init__("stop")

    def get_description(self) -> str:
        return "Stop a service"

    def execute(self, args):
        instance_id = args.service
        service = Service.from_id(instance_id)
        service.stop()

    def register_parser(self, subparsers) -> ArgumentParser:
        parser = super().register_parser(subparsers)
        parser.add_argument("service", help="The service to stop")
        return parser
