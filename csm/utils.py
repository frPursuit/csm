import os.path
import shutil
import string
import subprocess
from configparser import ConfigParser
from typing import Optional, IO

import yaml

VALID_NAME_CHARACTERS = string.ascii_letters + string.digits + ".-_"


def get_illegal_name_char(name: str) -> Optional[str]:
    for char in name:
        if char not in VALID_NAME_CHARACTERS:
            return char
    return None


def is_valid_name(name: str) -> bool:
    return get_illegal_name_char(name) is None


def create_ini_parser() -> ConfigParser:
    parser = ConfigParser(interpolation=None)
    parser.optionxform = str  # Keys should be case sensitive
    return parser


def from_yaml(text: str) -> any:
    return yaml.safe_load(text)


def to_yaml(element: any) -> str:
    return "---\n" + yaml.dump(element, sort_keys=False)


def run(*command: str, env: Optional[dict] = None):
    result = subprocess.run(command, env=get_all_env_vars(env))
    if result.returncode != 0:
        raise ChildProcessError(f"Unable to execute command (return code {result.returncode}): {command_to_string(*command)}")


def run_silent(*command: str, env: Optional[dict] = None):
    result = subprocess.run(command, env=get_all_env_vars(env), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    if result.returncode != 0:
        raise ChildProcessError(f"Unable to execute command (return code {result.returncode}): {command_to_string(*command)}")


def get(*command: str, env: Optional[dict] = None) -> str:
    result = subprocess.run(command, env=get_all_env_vars(env), stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    if result.returncode != 0:
        raise ChildProcessError(f"Unable to execute command (return code {result.returncode}): {command_to_string(*command)}")
    return result.stdout.decode("utf8")


def get_all_env_vars(env: Optional[dict] = None) -> dict:
    env_vars = {}
    for key, value in os.environ.items():
        env_vars[key] = value

    if env is not None:
        for key, value in env.items():
            env_vars[key] = value

    return env_vars


def command_to_string(*command: str) -> str:
    formatted = []
    for part in command:
        if " " in part:
            formatted.append(f"\"{part}\"")
        else: formatted.append(part)
    return " ".join(formatted)


# Perform a path join from an absolute parent, while ensuring the resulting path stays inside the parent
def absolute_path_join(parent: str, child: str) -> str:
    if not parent.startswith('/'):
        raise ValueError(f"'{parent}' is not an absolute path")

    while True:
        while child.startswith('/'):
            child = child[1:]
        result = os.path.join(parent, child)
        rel = os.path.relpath(result, parent)
        if not rel.startswith("../"):
            return result  # The result is safe

        # If the result is invalid, try to transform it to make it valid
        while rel.startswith("../"):
            rel = rel[3:]
        child = rel


def copy_stream(from_stream: IO, to_stream: IO, buffer_size: int = 4096):  # Move to PursuitLib
    while True:
        buffer = from_stream.read(buffer_size)
        # If the end of the stream has been reached, stop the loop
        if len(buffer) == 0:
            break
        to_stream.write(buffer)


def chown(path: str, user: str, group: str):
    if len(user) > 0 and user[0] in string.digits:
        user = int(user)
    if len(group) > 0 and group[0] in string.digits:
        group = int(group)
    shutil.chown(path, user, group)
