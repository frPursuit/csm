from csm.databases.localpostgresqlprovider import LocalPostgresqlProvider


class DatabaseProviders:
    # Local providers
    LOCAL_POSTGRESQL = LocalPostgresqlProvider()
