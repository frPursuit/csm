from csm import utils
from csm.databases.databaseprovider import DatabaseProvider
from csm.service.service import Service


class PostgresqlProvider(DatabaseProvider):
    def __init__(self, provider_type: str):
        super().__init__("postgresql", provider_type)

    def export_database(self, service: Service, name: str, target: str):
        db_name = self.get_database_name(service, name)
        # Export the data without the ACL/ownership information
        self.get_with_creds(service, "pg_dump",
                            "--format=custom", "--blobs", "--no-owner", "--no-privileges",
                            "-d", db_name, "-f", target)

    def import_database(self, service: Service, name: str, source: str):
        db_name = self.get_database_name(service, name)
        # Import the data without the ACL/ownership information
        self.get_with_creds(service, "pg_restore",
                            "--single-transaction", "--no-owner", "--no-privileges",
                            "-d", db_name, source)

    def cleanup(self, service: Service):
        user_name = self.get_user_name(service)
        self.delete_user(user_name)

    def psql(self, service: Service, command: str) -> str:
        return self.get_with_creds(service, "psql", "-c", command)

    def get_with_creds(self, service: Service, executable: str, *command: str):
        # Get the host address as seen from the controller
        host = self.get_host(service, True)
        user = self.get_user_name(service)
        password = self.get_user_password(service)
        return utils.get(executable, "--host", host, "--username", user, *command,
                         env={"PGPASSWORD": password})

