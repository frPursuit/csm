from csm import utils
from csm.databases.superpostgresqlprovider import SuperPostgresqlProvider
from csm.service.service import Service


class LocalPostgresqlProvider(SuperPostgresqlProvider):
    def __init__(self):
        super().__init__("local")

    def get_host(self, service: Service, from_controller: bool = False) -> str:
        if from_controller:
            # From the controller, the localhost interface is always available
            return "127.0.0.1"
        else:
            # From the service, only the network's gateway address is reachable
            return service.get_ipam().get_gateway_address().get_address()

    def super_psql(self, command: str) -> str:
        return utils.get("sudo", "-u", "postgres", "psql", "-c", command)
