from typing import List

from csm.databases.postgresqlprovider import PostgresqlProvider
from csm.service.service import Service
from pursuitlib import errors


class SuperPostgresqlProvider(PostgresqlProvider):
    """PostgreSQL provider with access to the privileged CLI"""

    def create_database(self, service: Service, name: str):
        user_name = self.get_user_name(service)
        if user_name not in self.get_users():
            self.create_user(user_name, self.get_user_password(service))

        internal_name = self.get_database_name(service, name)
        self.super_psql(f"CREATE DATABASE {internal_name} OWNER {user_name};")

    def delete_database(self, service: Service, name: str):
        internal_name = self.get_database_name(service, name)
        self.super_psql(f"DROP DATABASE {internal_name};")

    def get_databases(self, service: Service, *names: str) -> List[str]:
        try:
            databases = self.parse_table(self.super_psql("\\l"))
            result = []

            for name in names:
                internal_name = self.get_database_name(service, name)
                if internal_name in databases:
                    result.append(name)

            return result
        except:  # If an error occured, it means no databases are available
            return []

    def create_user(self, name: str, password: str):
        self.super_psql(f"CREATE USER {name} WITH ENCRYPTED PASSWORD '{password}';")

    def delete_user(self, name: str):
        self.super_psql(f"DROP USER {name};")

    def get_users(self) -> List[str]:
        return self.parse_table(self.super_psql("\\du"))

    def parse_table(self, output: str) -> List[str]:
        lines = output.splitlines()
        items = []
        for line in lines[3:]:
            if '|' not in line:
                continue
            item = line.split('|')[0].strip()
            if len(item) == 0:
                continue  # Ignore empty items
            items.append(item)
        return items

    def super_psql(self, command: str) -> str:
        raise errors.not_implemented(self, self.super_psql)
