from typing import List, TYPE_CHECKING

from pursuitlib import errors

if TYPE_CHECKING:
    from csm.service.service import Service


class DatabaseProvider:
    PASSWORD_LENGTH = 20

    def __init__(self, db_type: str, provider_type: str):
        self.db_type = db_type
        self.provider_type = provider_type

    def get_db_type(self) -> str:
        return self.db_type

    def get_provider_type(self) -> str:
        return self.provider_type

    def get_name(self) -> str:
        return f"{self.get_db_type()}_{self.get_provider_type()}"

    def create_database(self, service: "Service", name: str):
        raise errors.not_implemented(self, self.create_database)

    def delete_database(self, service: "Service", name: str):
        raise errors.not_implemented(self, self.delete_database)

    def export_database(self, service: "Service", name: str, target: str):
        raise errors.not_implemented(self, self.export_database)

    def import_database(self, service: "Service", name: str, source: str):
        raise errors.not_implemented(self, self.import_database)

    # Delete non-database objects related to this service (such as users)
    def cleanup(self, service: "Service"):
        pass

    # Return a list of all existing databases out of the ones provided
    # for the specified service
    def get_databases(self, service: "Service", *names: str) -> List[str]:
        raise errors.not_implemented(self, self.get_databases)

    def exists(self, service: "Service", name: str) -> bool:
        return name in self.get_databases(service, name)

    def create_user(self, name: str, password: str):
        raise errors.not_implemented(self, self.create_user)

    def delete_user(self, name: str):
        raise errors.not_implemented(self, self.delete_user)

    def get_users(self) -> List[str]:
        raise errors.not_implemented(self, self.get_users)

    def get_user_name(self, service: "Service") -> str:
        return f"csm_{service.get_instance_id()}"

    def get_user_password(self, service: "Service") -> str:
        secrets = service.get_secrets()
        password = secrets.get_secret(f"csm_{self.get_db_type()}_password", DatabaseProvider.PASSWORD_LENGTH)
        if secrets.is_dirty():
            secrets.save()
        return password

    def get_database_name(self, service: "Service", name: str) -> str:
        return f"csm_{service.get_instance_id()}_{name}"

    def get_host(self, service: "Service", from_controller: bool = False) -> str:
        raise errors.not_implemented(self, self.get_host)

    def get_context_vars(self, service: "Service", name: str) -> dict:
        return {
            "host": self.get_host(service),
            "name": self.get_database_name(service, name),
            "user": self.get_user_name(service),
            "password": self.get_user_password(service)
        }
