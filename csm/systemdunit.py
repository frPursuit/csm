from csm import utils
from csm.states.state import State
from csm.states.systemdstate import SystemdState


class SystemdUnit:
    @staticmethod
    def reload_daemons():
        utils.run("systemctl", "daemon-reload")

    def __init__(self, name: str):
        self.name = name

    def get_name(self) -> str:
        return self.name

    def is_enabled(self) -> bool:
        try:
            return utils.get("systemctl", "is-enabled", self.name).lower().startswith("enabled")
        except ChildProcessError:
            return False

    def is_active(self) -> bool:
        try:
            return utils.get("systemctl", "is-active", self.name).lower().startswith("active")
        except ChildProcessError:
            return False

    def get_state(self) -> State:
        return SystemdState.get(self.is_enabled(), self.is_active())

    def enable(self):
        utils.run("systemctl", "enable", self.name)

    def disable(self):
        utils.run("systemctl", "disable", self.name)

    def start(self):
        utils.run("systemctl", "start", self.name)

    def stop(self):
        utils.run("systemctl", "stop", self.name)

    def restart(self):
        utils.run("systemctl", "restart", self.name)
