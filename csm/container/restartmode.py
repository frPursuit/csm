class RestartMode:
    NO = "no"
    ON_FAILURE = "on-failure"
    ALWAYS = "always"
    UNLESS_STOPPED = "unless-stopped"

    def on_failure(self, retries: int) -> str:
        return f"on-failure:{retries}"
