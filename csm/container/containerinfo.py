from typing import Optional

from csm.states.containerstate import ContainerState
from csm.states.state import State


class ContainerInfo:
    def __init__(self, name: str, info: Optional[dict] = None):
        self.name = name
        self.state = ContainerState.NOT_CREATED
        self.status = self.state.get_name()
        self.image = None
        self.docker_name = None
        self.command = None

        if info is not None:
            self.state = ContainerState.get(info["State"])
            self.status = info["Status"]
            self.image = info["Image"]
            self.docker_name = info["Name"]
            self.command = info["Command"]

    def get_name(self) -> str:
        return self.name

    def get_state(self) -> State:
        return self.state

    def get_status(self) -> str:
        return self.status

    def get_image(self) -> Optional[str]:
        return self.image

    def get_docker_name(self) -> Optional[str]:
        return self.docker_name

    def get_command(self) -> Optional[str]:
        return self.command

    def __str__(self):
        return self.name
