import copy
import json
import os
from typing import TYPE_CHECKING, Optional, List, Dict, Tuple

from jinja2 import Template
from pursuitlib.iterators import citer

from csm import utils
from csm.container.containerinfo import ContainerInfo
from csm.container.containerpath import ContainerPath
from csm.container.restartmode import RestartMode
from csm.service.serviceconfig import ServiceConfig
from csm.service.serviceipam import ServiceIPAM
from csm.utils import is_valid_name
from csm.volume.volume import Volume
from csm.volume.volumetypes import VolumeTypes

if TYPE_CHECKING:
    from csm.service.service import Service


class ContainerSet:
    MAIN_SET_NAME = "main"
    MAIN_NETWORK_NAME = "main"

    @staticmethod
    def get_project_name_for(service: "Service", set_name: str) -> str:
        return f"csm_{service.get_instance_id()}_{set_name}"

    def __init__(self, service: "Service", name: str, path: str, containers: Optional[dict] = None, restart_mode: str = RestartMode.NO):
        self.service = service
        self.name = name
        self.path = path
        self.containers = containers
        self.restart_mode = restart_mode

    def get_name(self) -> str:
        return self.name

    def get_containers(self) -> List[str]:
        if self.containers is None:
            return []
        return list(self.containers.keys())

    def has_container(self, container: str) -> bool:
        if self.containers is None:
            return False
        return container in self.containers

    def get_volumes(self) -> Dict[str, Volume]:
        volumes = {}
        if self.containers is None:
            return volumes

        for container, spec in self.containers.items():
            for volume in spec.get("volumes", []):
                volume_def = ContainerPath.parse(volume)
                name = volume_def.get_relative_to()
                path = volume_def.get_internal_path()
                options = volume_def.get_metadata()

                if not is_valid_name(name):
                    raise ValueError(f"Invalid volume name: '{name}'")
                volumes[name] = Volume(name, self.service, VolumeTypes.DATA, ContainerPath.relative_to_container(container, path), options)

        return volumes

    def update_ipam(self, ipam: ServiceIPAM, update_from: "ContainerSet"):
        # Ensure containers have assigned addresses
        for container in self.get_containers():
            if not is_valid_name(container):
                raise ValueError(f"Invalid container name: '{container}'")
            ipam.get_address_for(container)

        # Free obsolete addresses
        for container in update_from.get_containers():
            if not self.has_container(container):
                ipam.free_address(container)

    def exists(self) -> bool:
        return os.path.isfile(self.path)

    def setup(self):
        if self.containers is None:
            raise ValueError("Cannot generate container set manifest: containers not specified")

        parent_dir = os.path.dirname(self.path)
        os.makedirs(parent_dir, exist_ok=True)

        config = self.service.get_config()
        config_context = self.service.get_context_vars()
        ipam = self.service.get_ipam()

        compose = {
            "services": copy.deepcopy(self.containers),
            "networks": {
                ContainerSet.MAIN_NETWORK_NAME: {
                    "name": self.service.get_network_name(),
                    "external": True
                }
            }
        }

        for name, spec in compose["services"].items():
            # Set network configuration
            spec["networks"] = {
                ContainerSet.MAIN_NETWORK_NAME: {
                    "ipv4_address": ipam.get_address_for(name).get_address()
                }
            }

            # Define environment variables
            env_vars = []
            for key, value in self.get_environment_variables(config, config_context, name, self.containers[name]).items():
                env_vars.append(f"{key}={value}")
            spec["environment"] = env_vars

            # Remove environment files
            if "env_file" in spec:
                del spec["env_file"]

            # Remove previously defined volumes
            if "volumes" in spec:
                del spec["volumes"]

            # Set restart mode
            spec["restart"] = self.restart_mode

        # Set up volumes defined by all components for this ContainerSet
        volumes = self.service.get_volumes()
        for volume in volumes.values():
            container = volume.get_container()
            if container not in compose["services"]:
                continue  # If a volume isn't associated with this ContainerSet, ignore it

            volume.setup_external_dir()
            container_spec = compose["services"][container]

            if "volumes" not in container_spec:
                container_spec["volumes"] = []
            container_spec["volumes"].append(volume.get_definition(docker_options=True))

        with open(self.path, "w") as compose_file:
            compose_file.write(utils.to_yaml(compose))
        os.chmod(self.path, 0o600)  # Protecting this file as it may contains secrets

        # Save the IPAM configuration if it has changed
        if ipam.is_dirty():
            ipam.save()

    def get_environment_variables(self, config: ServiceConfig, context: dict, container: str, container_spec: dict) -> dict:
        env_vars = copy.deepcopy(container_spec.get("environment", {}))
        env_vars |= config.get_env_config(container)

        # Replace the context variables with their value
        for key in env_vars.keys():
            template = Template(str(env_vars[key]))
            env_vars[key] = template.render(**context)

        return env_vars

    def create(self, daemon: bool = False):
        if daemon:
            self.execute_compose_cmd("up", "-d", "--wait")
        else: self.execute_compose_cmd("up")

    def destroy(self):
        self.execute_compose_cmd("down")

    def get_containers_info(self) -> List[ContainerInfo]:
        containers = []

        # Add active containers
        if self.exists():
            ps = self.get_compose_list("ps", "--all")
            for info in ps:
                name = info["Service"]
                containers.append(ContainerInfo(name, info))

        # Add inactive containers
        for name in self.get_containers():
            if citer(containers).filter(lambda c: c.get_name() == name).first_or_default() is None:
                containers.append(ContainerInfo(name))

        return list(sorted(containers, key=lambda c: c.get_name()))

    def get_project_name(self) -> str:
        return ContainerSet.get_project_name_for(self.service, self.get_name())

    def execute_compose_cmd(self, *command: str):
        if not self.exists():
            raise IOError(f"Container set '{self.get_name()}' does not exist")
        utils.run(*self.get_compose_cmd(*command))

    def get_compose_list(self, *command: str) -> list:
        if not self.exists():
            raise IOError(f"Container set '{self.get_name()}' does not exist")
        results = utils.get(*self.get_compose_cmd(*command, "--format", "json")).splitlines()
        return [json.loads(result) for result in results]

    def get_compose_cmd(self, *command: str) -> Tuple[str]:
        return ("docker", "compose", "--project-name", self.get_project_name(), "--file", self.path) + command
