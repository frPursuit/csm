import string
from typing import Optional


class ContainerPath:
    @staticmethod
    def parse(text: str) -> "ContainerPath":
        if ':' not in text:
            raise ValueError(f"Invalid container path: {text}")
        index = text.index(':')
        relative_to = text[:index]

        remaining = text[index+1:]
        if ':' in remaining:
            index = remaining.index(':')
            path = remaining[:index]
            metadata = remaining[index+1:]
            return ContainerPath(relative_to, path, metadata)
        else: return ContainerPath(relative_to, remaining)

    @staticmethod
    def tcp_port(container: str, port: int) -> "ContainerPath":
        return ContainerPath(container, str(port))

    @staticmethod
    def relative_to_container(container: str, path: str) -> "ContainerPath":
        if not path.startswith('/'):
            raise ValueError(f"Invalid absolute container path: {path}")
        return ContainerPath(container, path)

    @staticmethod
    def relative_to_volume(volume: str, path: str) -> "ContainerPath":
        return ContainerPath(f"#{volume}", path)

    def __init__(self, relative_to: str, path: str, metadata: Optional[str] = None):
        self.relative_to = relative_to
        self.path = path
        self.metadata = metadata

    def get_relative_to(self) -> str:
        return self.relative_to

    def get_internal_path(self) -> str:
        return self.path

    def is_tcp_port(self) -> bool:
        return self.is_relative_to_container() and self.path[0] in string.digits

    def is_relative_to_container(self) -> bool:
        return not self.is_relative_to_volume()

    def is_relative_to_volume(self) -> bool:
        return self.get_relative_to().startswith('#')

    def get_tcp_port(self) -> int:
        return int(self.get_internal_path())

    def get_volume_name(self) -> str:
        if not self.is_relative_to_volume():
            raise ValueError(f"'{self}' is not relative to a volume")
        return self.get_relative_to()[1:]

    def has_metadata(self) -> bool:
        return self.metadata is not None

    def get_metadata(self) -> Optional[str]:
        return self.metadata

    def __str__(self) -> str:
        return f"{self.relative_to}:{self.path}"
