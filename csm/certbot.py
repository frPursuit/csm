import os.path
import shutil

from pursuitlib.utils import is_null_or_empty

from csm import utils
from csm.config import Config


class Certbot:
    CERTIFICATES_DIR = "/etc/letsencrypt/live"
    CERTIFICATE_FILE = "fullchain.pem"
    CERTIFICATE_KEY_FILE = "privkey.pem"
    RENEWAL_DIR = "/etc/letsencrypt/renewal"
    ARCHIVE_DIR = "/etc/letsencrypt/archive"

    @staticmethod
    def generate_certificate(domain: str):
        config = Config.get_letsencrypt_config()
        for item in ["email", "certbot_arguments"]:
            if is_null_or_empty(config[item]):
                raise KeyError(f"Item '{item}' from section [{Config.LETSENCRYPT_SECTION}] of CSM config is unspecified")

        email = config["email"]
        args = config["certbot_arguments"].split(' ')
        utils.run("certbot", "certonly", *args, "--non-interactive", "--agree-tos", "--email", email, "-d", domain)

    @staticmethod
    def delete_certificate(domain: str):
        renewal = os.path.join(Certbot.RENEWAL_DIR, f"{domain}.conf")
        if os.path.isfile(renewal):
            os.remove(renewal)

        for parent in [Certbot.CERTIFICATES_DIR, Certbot.ARCHIVE_DIR]:
            directory = os.path.join(parent, domain)
            if os.path.isdir(directory):
                shutil.rmtree(directory)

    @staticmethod
    def certificate_exists(domain: str):
        return os.path.isfile(Certbot.get_certificate_path(domain)) and\
               os.path.isfile(Certbot.get_certificate_key_path(domain))

    @staticmethod
    def get_certificate_path(domain: str):
        return os.path.join(Certbot.CERTIFICATES_DIR, domain, Certbot.CERTIFICATE_FILE)

    @staticmethod
    def get_certificate_key_path(domain: str):
        return os.path.join(Certbot.CERTIFICATES_DIR, domain, Certbot.CERTIFICATE_KEY_FILE)
