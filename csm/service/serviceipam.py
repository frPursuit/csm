import os
from typing import Dict, TYPE_CHECKING

from csm import utils
from csm.config import Config
from csm.directories import Directories
from csm.hosts import Hosts
from csm.ipam.ipaddress import IPAddress
from csm.ipam.ipallocation import IPAllocation
from csm.ipam.ipnetwork import IPNetwork
from csm.ipam.ipprefix import IPPrefix

if TYPE_CHECKING:
    from csm.service.service import Service


class ServiceIPAM:
    IPAM_FILE = "ipam.yml"

    # This get the current IP allocation based on IPAM configurations saved on disk (NOT on the ones in memory)
    @staticmethod
    def get_global() -> IPAllocation:
        ipam = IPAllocation(IPPrefix.parse_cidr(Config.get_ip_pool()))

        if not os.path.isdir(Directories.SERVICES_DATA):
            return ipam

        for item in os.listdir(Directories.SERVICES_DATA):
            ipam_path = os.path.join(Directories.SERVICES_DATA, item, ServiceIPAM.IPAM_FILE)
            if not os.path.isfile(ipam_path):
                continue
            with open(ipam_path, "r") as ipam_file:
                item_ipam = utils.from_yaml(ipam_file.read())
            if "network" not in item_ipam:
                continue

            network = IPPrefix.parse_cidr(item_ipam["network"])
            if network in ipam.get_pool():
                ipam.mark_allocated(network)

        return ipam

    def __init__(self, service: "Service"):
        self.service = service
        self.network = None
        self.addresses: Dict[str, IPAddress] = {}
        self.dirty = False

        path = self.get_path()
        if os.path.isfile(path):
            with open(path, "r") as file:
                data = utils.from_yaml(file.read())
                if "network" in data:
                    self.network = IPNetwork(IPPrefix.parse_cidr(data["network"]))
                if "addresses" in data:
                    network_size = self.network.get_size() if self.network is not None else Config.get_service_network_size()
                    for name, address in data["addresses"].items():
                        self.addresses[name] = IPAddress(IPAddress.parse_address(address), network_size)

        # Allocate the network if it hasn't been done already
        if self.network is None:
            global_ipam = ServiceIPAM.get_global()
            self.network = IPNetwork(global_ipam.allocate_prefix(Config.get_service_network_size()))
            self.dirty = True

        # The gateway is always the first IP of the network
        self.gateway = self.network.allocate_address(self.network.get_size())
        for ip in self.addresses.values():
            self.network.mark_allocated(ip)

    def get_network_address(self) -> IPAddress:
        return self.network.get_address()

    def get_gateway_address(self) -> IPAddress:
        # The gateway address is always the first usable address of the network
        network_addr = self.network.get_address().get_address_bytes()
        return IPAddress((network_addr[0], network_addr[1], network_addr[2], network_addr[3] + 1), self.network.get_size())

    def get_address_for(self, name: str) -> IPAddress:
        if name not in self.addresses:
            self.addresses[name] = self.network.allocate_address()
            Hosts.instance.set(self.service.get_domain(name), self.addresses[name].get_address())
            self.dirty = True
        return self.addresses[name]

    def free_address(self, name: str):
        if name not in self.addresses:
            return
        address = self.addresses[name]
        del self.addresses[name]
        self.network.free(address)
        Hosts.instance.delete(self.service.get_domain(name))
        self.dirty = True

    def get_path(self) -> str:
        return os.path.join(self.service.get_data_directory(), ServiceIPAM.IPAM_FILE)

    def is_dirty(self) -> bool:
        return self.dirty

    def save(self):
        content = {
            "network": str(self.network),
            "addresses": {}
        }
        for name, address in self.addresses.items():
            content["addresses"][name] = address.get_address()

        path = self.get_path()
        parent_dir = os.path.dirname(path)
        os.makedirs(parent_dir, exist_ok=True)
        with open(self.get_path(), "w") as file:
            file.write(utils.to_yaml(content))

        self.dirty = False

        # Save the hosts file with the IPAM config
        if Hosts.instance.is_dirty():
            Hosts.instance.save()
