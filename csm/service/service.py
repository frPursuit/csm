import json
import os
import os.path
import shutil
from pathlib import Path
from tempfile import mkdtemp
from typing import List, Optional, Tuple, TYPE_CHECKING, Dict

import requests
import yaml

from csm import utils
from csm.components.component import Component
from csm.components.components import Components
from csm.container.containerpath import ContainerPath
from csm.container.containerset import ContainerSet
from csm.directories import Directories
from csm.registry import Registry
from csm.service.serviceimage import ServiceImage
from csm.states.servicestate import ServiceState
from csm.states.state import State
from csm.utils import is_valid_name, get_illegal_name_char
from csm.volume.volume import Volume
from csm.volume.volumetypes import VolumeTypes
from pursuitlib.console import console
from pursuitlib.console.textcomponent import TextComponent
from pursuitlib.decorators import cached

if TYPE_CHECKING:
    from csm.service.serviceconfig import ServiceConfig
    from csm.service.serviceipam import ServiceIPAM
    from csm.service.secrets import Secrets


class Service:
    EXPORT_SPEC = "service.yml"
    EXPORT_DATA_DIR = "data"
    EXPORT_CONFIG_DIR = "config"
    EXPORT_VOLUMES_SUBDIR = "volumes"

    @staticmethod
    def list_installed() -> List["Service"]:
        if not os.path.isdir(Directories.SERVICES_INSTALL):
            return []

        installed = []
        for item in os.listdir(Directories.SERVICES_INSTALL):
            # Only consider YAML files
            if not item.lower().endswith(".yml"):
                continue

            spec_path = os.path.join(Directories.SERVICES_INSTALL, item)
            if not os.path.isfile(spec_path):
                continue
            try:
                instance_id = Path(spec_path).stem
                installed.append(Service.from_file(spec_path, instance_id))
            except Exception as e:
                console.write_err_line(f"Unable to read '{spec_path}'")
                console.write_err_line(f"{e}")
        return installed

    @staticmethod
    def from_id(instance_id: str) -> "Service":
        service = Service.try_from_id(instance_id)
        if service is None:
            raise ValueError(f"Service '{instance_id}' not found.")
        return service

    @staticmethod
    def try_from_id(instance_id: str) -> Optional["Service"]:
        spec_path = os.path.join(Directories.SERVICES_INSTALL, f"{instance_id}.yml")
        if not os.path.isfile(spec_path):
            return None
        return Service.from_file(spec_path, instance_id)

    @staticmethod
    def from_text(text: str, instance_id: Optional[str] = None) -> "Service":
        spec = yaml.safe_load(text)
        if instance_id is None:
            # If "id" is missing, do not fail here. An exception will be raised elsewhere
            instance_id = spec.get("id", "")
        return Service(instance_id, spec)

    @staticmethod
    def from_file(path: str, instance_id: Optional[str] = None) -> "Service":
        with open(path, "r") as spec_file:
            text = spec_file.read()
        return Service.from_text(text, instance_id)

    @staticmethod
    def from_url(url: str, instance_id: Optional[str] = None) -> "Service":
        text = requests.get(url).text
        return Service.from_text(text, instance_id)

    @staticmethod
    def import_from(source: str, instance_id: str) -> "Service":
        from csm.service.secrets import Secrets

        if not ServiceImage.key_exists():
            raise IOError("No image key exists")

        temp_dir = mkdtemp(prefix="csm-import-")
        # Making the directory world-executable so the DB can be correctly imported
        os.chmod(temp_dir, 0o755)

        try:
            ServiceImage.extract(ServiceImage.get_key(), source, temp_dir)

            # Parse service spec
            spec_file = os.path.join(temp_dir, Service.EXPORT_SPEC)
            if not os.path.isfile(spec_file):
                raise IOError("Unable to find service spec inside exported archive")
            service = Service.from_file(spec_file, instance_id)

            # Import secrets before install
            imported_secrets = os.path.join(temp_dir, Secrets.SECRETS_FILE)
            if os.path.isfile(imported_secrets):
                Secrets(service, imported_secrets).save()

            # Import data volumes before install
            imported_data = os.path.join(temp_dir, Service.EXPORT_DATA_DIR)
            imported_volumes = os.path.join(imported_data, Service.EXPORT_VOLUMES_SUBDIR)
            if os.path.isdir(imported_volumes):
                data_dir = service.get_data_directory()
                os.makedirs(data_dir, exist_ok=True)
                shutil.move(imported_volumes, os.path.join(data_dir, VolumeTypes.DATA.get_sub_dir()))

            # Import config before install
            imported_config = os.path.join(temp_dir, Service.EXPORT_CONFIG_DIR)
            if os.path.isdir(imported_config):
                config_dir = service.get_config_directory()
                parent_dir = os.path.dirname(config_dir)
                os.makedirs(parent_dir, exist_ok=True)
                shutil.move(imported_config, config_dir)

            # Install service
            service.install()

            # Import component-specific data
            for component in Registry.get_components():
                data = service.get_component_data(component)
                if data is None:
                    continue
                component.import_data(service, data, os.path.join(imported_data, component.get_name()))

            return service
        finally:
            shutil.rmtree(temp_dir)

    def __init__(self, instance_id: str, spec: dict):
        self.instance_id = instance_id
        self.spec = spec

        for component in Registry.get_required_components():
            cname = component.get_name().lower()
            if cname not in self.spec:
                raise ValueError(f"The '{cname}' attribute is required in a Service spec")

    def get_instance_id(self) -> str:
        return self.instance_id

    def get_service_id(self) -> str:
        return self.get_component_data(Components.ID)

    def get_spec_path(self) -> str:
        return os.path.join(Directories.SERVICES_INSTALL, f"{self.get_instance_id()}.yml")

    def get_config_directory(self) -> str:
        return os.path.join(Directories.SERVICES_CONFIG, self.get_instance_id())

    def get_data_directory(self) -> str:
        return os.path.join(Directories.SERVICES_DATA, self.get_instance_id())

    def get_component_data(self, component: Component) -> any:
        cname = component.get_name().lower()
        return self.spec[cname] if cname in self.spec else None

    @cached
    def get_config(self) -> "ServiceConfig":
        from csm.service.serviceconfig import ServiceConfig
        return ServiceConfig(self)

    def get_default_config(self) -> dict:
        default_config = {}

        for component in Registry.get_components():
            data = self.get_component_data(component)
            if data is None:
                continue
            default = component.get_default_config(self, data)
            if default is None:
                continue
            default_config[component.get_name()] = default

        additional_config = self.get_component_data(Components.CONFIG)
        if additional_config is not None:
            for section, entries in additional_config.items():
                if section not in default_config:
                    default_config[section] = {}
                for key, value in entries.items():
                    default_config[section][key] = value

        return default_config

    @cached
    def get_ipam(self) -> "ServiceIPAM":
        from csm.service.serviceipam import ServiceIPAM
        return ServiceIPAM(self)

    def get_domain(self, container: str) -> str:
        return f"{container}.{self.get_instance_id()}.svc.local"

    @cached
    def get_secrets(self) -> "Secrets":
        from csm.service.secrets import Secrets
        return Secrets(self)

    @cached
    def get_context_vars(self) -> dict:
        context = {}

        # Set component config context
        for component in Registry.get_components():
            data = self.get_component_data(component)
            if data is None:
                continue
            component_ctx = component.get_context_vars(self, data)
            if component_ctx is not None:
                context[component.get_name()] = component_ctx

        # Set global context variables
        context["config"] = self.get_config().get_context_var()
        context["secrets"] = self.get_secrets().get_all()
        return context

    @cached
    def get_volumes(self) -> Dict[str, Volume]:
        volumes: Dict[str, Volume] = {}
        for component in Registry.get_components():
            data = self.get_component_data(component)
            if data is None:
                continue
            for name, volume in component.get_volumes(self, data).items():
                if not is_valid_name(name):
                    raise ValueError(f"Invalid volume name '{name}': found illegal character '{get_illegal_name_char(name)}'")
                if name in volumes:
                    raise ValueError(f"Volume '{name}' created by '{component.get_name()}' already exists")
                volumes[name] = volume
        return volumes

    def get_host_path(self, path: ContainerPath, volumes: Optional[Dict[str, Volume]] = None) -> str:
        if volumes is None:  # Volumes can be provided as an argument to avoid listing them everytime
            volumes = self.get_volumes()

        if not path.is_relative_to_volume():
            raise ValueError(f"The container path {path} does not have a host path")
        volume_name = path.get_volume_name()
        internal_path = path.get_internal_path()

        if volume_name not in volumes:
            raise ValueError(f"Unknown volume: {volume_name}")
        volume = volumes[volume_name]
        return volume.get_host_path(internal_path)

    def get_main_container_set(self) -> ContainerSet:
        containers = self.get_component_data(Components.CONTAINERS)
        return Components.CONTAINERS.get_container_set(self, containers)

    def get_state(self) -> State:
        states = []
        for component in Registry.get_components():
            data = self.get_component_data(component)
            if data is None:
                continue
            states.append(component.get_state(self, data))
        return ServiceState.combine(*states)

    def list_changes_for(self, component: Component, update_from: Optional["Service"] = None) -> List[TextComponent]:
        data = self.get_component_data(component)
        old_data = update_from.get_component_data(component) if update_from is not None else None

        if data is None and old_data is None:
            return []
        return component.list_changes(self, data, old_data)

    def list_changes(self, update_from: Optional["Service"] = None) -> List[Tuple[Component, List[TextComponent]]]:
        changes = []
        for component in Registry.get_components():
            component_changes = self.list_changes_for(component, update_from)
            if len(component_changes) > 0:
                changes.append((component, component_changes))
        return changes

    def install_component(self, component: Component, update_from: Optional["Service"] = None):
        data = self.get_component_data(component)
        old_data = update_from.get_component_data(component) if update_from is not None else None

        if data is None and old_data is None:
            return
        component.update(self, data, old_data)

    def install(self, existing: "Service" = None):
        spec_path = self.get_spec_path()
        spec_dir = os.path.dirname(spec_path)
        os.makedirs(spec_dir, exist_ok=True)

        if existing is None and os.path.isfile(spec_path):
            existing = Service.from_id(self.get_instance_id())

        # Update the service's config by adding new default values
        default_config = self.get_default_config()
        config = self.get_config()
        config_dirty = False
        for section, entries in default_config.items():
            config_section = config.get_section(section)
            for key, value in entries.items():
                if key not in config_section:
                    config_section[key] = str(value)
                    config_dirty = True
        if config_dirty:
            config.save()

        # Update the service's components
        for component in Registry.get_components():
            self.install_component(component, existing)

        with open(spec_path, "w") as spec_file:
            spec_file.write(self.get_spec())

    def create_network(self):
        ipam = self.get_ipam()
        name = self.get_network_name()
        network = str(ipam.get_network_address())
        gateway = ipam.get_gateway_address().get_address()
        utils.run_silent("docker", "network", "create", "--subnet", network, "--gateway", gateway, name)

    def delete_network(self):
        utils.run_silent("docker", "network", "rm", self.get_network_name())

    def does_network_exist(self) -> bool:
        name = self.get_network_name()
        networks = utils.get("docker", "network", "ls", "--format", "json")
        for network_data in networks.splitlines():
            network = json.loads(network_data)
            if network["Name"] == name:
                return True
        return False

    def start_component(self, component: Component):
        data = self.get_component_data(component)
        if data is None:
            raise ValueError(f"Cannot start component '{component}' for service '{self}': the associated section doesn't exist in the service's spec")
        component.start(self, data)

    def start(self):
        """Start the service. This method is idempotent"""
        if not self.does_network_exist():
            self.create_network()

        for component in Registry.get_components():
            data = self.get_component_data(component)
            if data is None:
                continue
            component.start(self, data)

    def stop_component(self, component: Component):
        """Stop the service. This method is idempotent"""
        data = self.get_component_data(component)
        if data is None:
            raise ValueError(f"Cannot stop component '{component}' for service '{self}': the associated section doesn't exist in the service's spec")
        if ServiceState.is_active(component.get_state(self, data)):
            component.stop(self, data)

    def stop(self):
        for component in Registry.get_components():
            data = self.get_component_data(component)
            if data is None:
                continue
            component.stop(self, data)

        if self.does_network_exist():
            self.delete_network()

    def list_remove_changes_for(self, component: Component) -> List[TextComponent]:
        data = self.get_component_data(component)
        if data is None:
            return []
        return component.list_changes(self, None, data)

    def list_remove_changes(self) -> List[Tuple[Component, List[TextComponent]]]:
        changes = []
        for component in Registry.get_components():
            component_changes = self.list_remove_changes_for(component)
            if len(component_changes) > 0:
                changes.append((component, component_changes))
        return changes

    def remove_component(self, component: Component):
        data = self.get_component_data(component)
        if data is None:
            raise ValueError(f"Cannot remove component '{component}' for service '{self}': the associated section doesn't exist in the service's spec")
        component.update(self, None, data)

    def remove(self):
        for component in Registry.get_components():
            data = self.get_component_data(component)
            if data is None:
                continue
            component.update(self, None, data)

        spec_path = self.get_spec_path()
        if os.path.isfile(spec_path):
            os.remove(spec_path)

        shutil.rmtree(self.get_config_directory(), ignore_errors=True)
        shutil.rmtree(self.get_data_directory(), ignore_errors=True)

    def export_to(self, target: str):
        from csm.service.secrets import Secrets

        temp_dir = mkdtemp(prefix="csm-export-")
        # Making the directory world-executable so the DB can be correctly exported
        os.chmod(temp_dir, 0o755)

        try:
            content = {}

            # Export the service's spec
            spec_path = os.path.join(temp_dir, Service.EXPORT_SPEC)
            with open(spec_path, "w") as spec_file:
                spec_file.write(utils.to_yaml(self.spec))
            content[Service.EXPORT_SPEC] = spec_path

            # Export the service's secrets
            secrets_path = os.path.join(self.get_data_directory(), Secrets.SECRETS_FILE)
            if os.path.isfile(secrets_path):
                content[Secrets.SECRETS_FILE] = secrets_path

            # Export the service's data volumes
            data_volumes = os.path.join(self.get_data_directory(), VolumeTypes.DATA.get_sub_dir())
            if os.path.isdir(data_volumes):
                content[os.path.join(Service.EXPORT_DATA_DIR, Service.EXPORT_VOLUMES_SUBDIR)] = data_volumes

            # Export the service's configuration
            config_dir = self.get_config_directory()
            if os.path.isdir(config_dir):
                content[Service.EXPORT_CONFIG_DIR] = self.get_config_directory()

            # Export component-specific data
            exported_data = os.path.join(temp_dir, Service.EXPORT_DATA_DIR)
            os.mkdir(exported_data)
            for component in Registry.get_components():
                data = self.get_component_data(component)
                if data is None:
                    continue
                component.export_data(self, data, os.path.join(exported_data, component.get_name()))
            content[Service.EXPORT_DATA_DIR] = exported_data

            # Generate an image key if there isn't any
            if not ServiceImage.key_exists():
                ServiceImage.generate_key()
            ServiceImage.create(ServiceImage.get_key(), content, target)
        finally:
            shutil.rmtree(temp_dir)

    def get_network_name(self) -> str:
        return f"csm_{self.get_instance_id()}"

    def get_spec(self) -> str:
        return utils.to_yaml(self.spec)

    def __str__(self) -> str:
        return self.get_instance_id()
