import copy
import os
import secrets
import string
from typing import Optional

from csm import utils
from csm.service.service import Service


class Secrets:
    SECRETS_FILE = "secrets.yml"
    SECRET_SYMBOLS = string.ascii_letters + string.digits

    def __init__(self, service: Service, load_from: Optional[str] = None):
        self.service = service
        self.secrets = {}
        self.dirty = False

        path = load_from or self.get_path()
        if os.path.isfile(path):
            with open(path, "r") as secrets_file:
                data = utils.from_yaml(secrets_file.read())
            for key, value in data.items():
                self.secrets[key] = value
        else: self.dirty = True

    def get_secret(self, name: str, length: int) -> str:
        if name not in self.secrets:
            self.secrets[name] = "".join([secrets.choice(Secrets.SECRET_SYMBOLS) for _ in range(length)])
            self.dirty = True
        return self.secrets[name]

    def remove_secret(self, name: str):
        if name in self.secrets:
            del self.secrets[name]
            self.dirty = True

    def exists(self, name: str) -> bool:
        return name in self.secrets

    def get_all(self) -> dict:
        return copy.deepcopy(self.secrets)

    def get_path(self) -> str:
        return os.path.join(self.service.get_data_directory(), Secrets.SECRETS_FILE)

    def is_empty(self) -> bool:
        return len(self.secrets) == 0

    def is_dirty(self) -> bool:
        return self.dirty

    def save(self, save_to: Optional[str] = None):
        path = save_to or self.get_path()
        parent_dir = os.path.dirname(path)
        os.makedirs(parent_dir, exist_ok=True)
        with open(path, "w") as file:
            file.write(utils.to_yaml(self.secrets))
        os.chmod(path, 0o600)  # Protecting this file as it contains secrets

        self.dirty = False
