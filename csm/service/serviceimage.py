import os.path
import secrets
import subprocess
from typing import Dict

from csm import utils
from csm.directories import Directories


class ServiceImage:
    EXTENSION = "svi"
    MAGIC = b"SVI"
    FORMAT_VERSION = 1
    ENC_ALGORITHM = "aes-256-cbc"
    IV_LENGTH = 128  # In bytes
    KEY_SIZE = 256  # In bytes
    KEY_FILE = "image.key"

    @staticmethod
    def get_key_path() -> str:
        return os.path.join(Directories.DATA, ServiceImage.KEY_FILE)

    @staticmethod
    def key_exists() -> bool:
        return os.path.isfile(ServiceImage.get_key_path())

    @staticmethod
    def get_key() -> bytes:
        path = ServiceImage.get_key_path()
        with open(path, "rb") as key_file:
            key = key_file.read()

        size_bytes = ServiceImage.KEY_SIZE // 8
        if len(key) != size_bytes:
            raise IOError(f"Invalid image key size: {len(key)} instead of {size_bytes}")
        return key

    @staticmethod
    def set_key(key: bytes):
        size_bytes = ServiceImage.KEY_SIZE // 8
        if len(key) != size_bytes:
            raise ValueError(f"Invalid key size: {len(key)} instead of {size_bytes}")

        path = ServiceImage.get_key_path()
        with open(path, "wb") as key_file:
            key_file.write(key)
        os.chmod(path, 0o600)  # Protecting the key file

    @staticmethod
    def generate_key():
        ServiceImage.set_key(secrets.token_bytes(ServiceImage.KEY_SIZE // 8))

    @staticmethod
    def create(key: bytes, content: Dict[str, str], target: str):
        # Create compressed tar file
        path_transforms = []
        for name, item in content.items():
            if not item.startswith('/'):
                raise ValueError("Only absolute paths can be used with 'create_compressed_archive'")
            if item.endswith('/'):
                item = item[:-1]
            internal_name = item[1:]
            path_transforms += ["--transform", f"s|^{internal_name}|{name}|"]

        tar = subprocess.Popen(["tar", "-cz", *content.values(), *path_transforms], stdout=subprocess.PIPE,
                               stderr=subprocess.DEVNULL)

        # Encrypt tarfile
        iv = secrets.token_bytes(ServiceImage.IV_LENGTH // 8)
        openssl = subprocess.Popen(["openssl", "enc", "-e", f"-{ServiceImage.ENC_ALGORITHM}", "-K", key.hex(), "-iv", iv.hex()],
                                   stdin=tar.stdout, stdout=subprocess.PIPE,stderr=subprocess.DEVNULL)

        with open(target, "wb") as output:
            output.write(ServiceImage.MAGIC)
            output.write(str(ServiceImage.FORMAT_VERSION).encode("ascii"))
            output.write(iv)
            utils.copy_stream(openssl.stdout, output)

        returncode = tar.wait()
        if returncode != 0:
            raise ChildProcessError(f"Unable to create tar file: return code {returncode}")
        returncode = openssl.wait()
        if returncode != 0:
            raise ChildProcessError(f"Unable to encrypt tar file: return code {returncode}")

    @staticmethod
    def extract(key: bytes, file: str, target: str):
        with open(file, "rb") as data:
            magic = data.read(3)
            if magic != ServiceImage.MAGIC:
                raise IOError(f"Not a SVI file: {file}")
            version = data.read(1).decode("ascii")
            if version != str(ServiceImage.FORMAT_VERSION):
                raise IOError(f"Unsupported SVI format version: {version}")

            iv = data.read(ServiceImage.IV_LENGTH // 8)
            openssl = subprocess.Popen(["openssl", "enc", "-d", f"-{ServiceImage.ENC_ALGORITHM}", "-K", key.hex(), "-iv", iv.hex()],
                                       stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
            tar = subprocess.Popen(["tar", "-xz", "-C", target], stdin=openssl.stdout, stderr=subprocess.DEVNULL)
            utils.copy_stream(data, openssl.stdin)
            openssl.stdin.close()

            returncode = openssl.wait()
            if returncode != 0:
                raise ChildProcessError(f"Unable to decrypt tar file: return code {returncode}")
            returncode = tar.wait()
            if returncode != 0:
                raise ChildProcessError(f"Unable to extract tar file: return code {returncode}")
