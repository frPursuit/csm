import os.path
from configparser import SectionProxy
from typing import TYPE_CHECKING

from csm.utils import create_ini_parser

if TYPE_CHECKING:
    from csm.service.service import Service


class ServiceConfig:
    FILE = "service.cfg"

    def __init__(self, service: "Service"):
        self.service = service
        self.config = create_ini_parser()

        path = self.get_path()
        if os.path.isfile(path):
            self.config.read(path)

    def get_section(self, name: str) -> SectionProxy:
        if not self.config.has_section(name):
            self.config.add_section(name)
        return self.config[name]

    def get_env_config(self, container: str) -> SectionProxy:
        return self.get_section(f"env.{container}")

    def get_path(self) -> str:
        return os.path.join(self.service.get_config_directory(), ServiceConfig.FILE)

    def get_context_var(self) -> dict:
        ctx = {}
        for section_name in self.config.sections():
            section = self.get_section(section_name)
            section_var = {}
            for key in section:
                section_var[key] = section[key]
            ctx[section_name] = section_var
        return ctx

    def save(self):
        path = self.get_path()
        parent_dir = os.path.dirname(path)
        os.makedirs(parent_dir, exist_ok=True)
        with open(path, "w") as config_file:
            self.config.write(config_file)
