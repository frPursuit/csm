import os.path
from configparser import SectionProxy
from typing import Dict

from csm.directories import Directories
from csm.registry import Registry
from csm.utils import create_ini_parser


class Config:
    FILE = os.path.join(Directories.CONFIG, "global.cfg")

    NETWORK_SECTION = "network"
    NETWORK_DEFAULTS: Dict[str, any] = {
        "ip_pool": "10.44.0.0/16",
        "service_network_size": 24,
    }

    DB_PROVIDERS_SECTION = "db-providers"
    DEFAULT_DB_PROVIDER = "local"

    LETSENCRYPT_SECTION = "letsencrypt"
    LETSENCRYPT_ITEMS = ["email", "certbot_arguments"]

    verbose = False
    instance: "Config"

    @staticmethod
    def initialize():
        Config.instance = Config()

    @staticmethod
    def get_ip_pool() -> str:
        return Config.instance.config[Config.NETWORK_SECTION]["ip_pool"]

    @staticmethod
    def get_service_network_size() -> int:
        return int(Config.instance.config[Config.NETWORK_SECTION]["service_network_size"])

    @staticmethod
    def get_db_provider_type(db_type: str) -> str:
        try:
            return Config.instance.config[Config.DB_PROVIDERS_SECTION][db_type]
        except KeyError:
            raise KeyError(f"Invalid database type: {db_type}")

    @staticmethod
    def get_letsencrypt_config() -> SectionProxy:
        if not Config.instance.config.has_section(Config.LETSENCRYPT_SECTION):
            raise KeyError(f"Section [letsencrypt] absent from config file")
        return Config.instance.config[Config.LETSENCRYPT_SECTION]

    def __init__(self):
        self.config = create_ini_parser()
        if os.path.isfile(Config.FILE):
            self.config.read(Config.FILE)

        missing = False

        if not self.config.has_section(Config.NETWORK_SECTION):
            self.config.add_section(Config.NETWORK_SECTION)
        network = self.config[Config.NETWORK_SECTION]
        for key, value in Config.NETWORK_DEFAULTS.items():
            if key not in network:
                network[key] = str(value)
                missing = True

        if not self.config.has_section(Config.DB_PROVIDERS_SECTION):
            self.config.add_section(Config.DB_PROVIDERS_SECTION)
        db_providers = self.config[Config.DB_PROVIDERS_SECTION]
        for db_type in Registry.get_database_types():
            if db_type not in db_providers:
                db_providers[db_type] = Config.DEFAULT_DB_PROVIDER
                missing = True

        if not self.config.has_section(Config.LETSENCRYPT_SECTION):
            self.config.add_section(Config.LETSENCRYPT_SECTION)
        letsencrypt = self.config[Config.LETSENCRYPT_SECTION]
        for item in Config.LETSENCRYPT_ITEMS:
            if item not in letsencrypt:
                letsencrypt[item] = ""
                missing = True

        if missing:
            parent_dir = os.path.dirname(Config.FILE)
            os.makedirs(parent_dir, exist_ok=True)
            with open(Config.FILE, "w") as config_file:
                self.config.write(config_file)
