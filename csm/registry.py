from typing import List, Type, TYPE_CHECKING

from pursuitlib.iterators import citer

if TYPE_CHECKING:
    from csm.actions.action import Action
    from csm.components.component import Component
    from csm.databases.databaseprovider import DatabaseProvider


class Registry:
    instance: "Registry"

    @staticmethod
    def initialize():
        Registry.instance = Registry()

    @staticmethod
    def get_action(name: str) -> "Action":
        action = citer(Registry.instance.actions).filter(lambda a: a.get_name().lower() == name.lower() or name.lower() in a.get_aliases())
        try:
            return action.first()
        except StopIteration:
            raise KeyError(f"Unknown action: {name}")

    @staticmethod
    def get_actions() -> List["Action"]:
        return Registry.instance.actions

    @staticmethod
    def get_required_components() -> List["Component"]:
        return list(citer(Registry.instance.components).filter(lambda c: c.is_required()))

    @staticmethod
    def get_components() -> List["Component"]:
        return Registry.instance.components

    @staticmethod
    def get_database_provider(db_type: str, provider_type: str) -> "DatabaseProvider":
        provider = citer(Registry.instance.db_providers).filter(lambda p: p.get_db_type().lower() == db_type.lower() and p.get_provider_type().lower() == provider_type.lower())
        try:
            return provider.first()
        except StopIteration:
            raise KeyError(f"Unknown database provider for database '{db_type}': {provider_type}")

    @staticmethod
    def get_database_providers() -> List["DatabaseProvider"]:
        return Registry.instance.db_providers

    @staticmethod
    def get_database_types() -> List[str]:
        types = []
        for provider in Registry.get_database_providers():
            db_type = provider.get_db_type()
            if db_type not in types:
                types.append(db_type)
        return types

    @staticmethod
    def as_list(items: Type) -> list:
        lst = []

        for item in dir(items):
            # Ignore internal fields
            if item.startswith("_"):
                continue
            lst.append(getattr(items, item))

        return lst

    def __init__(self):
        from csm.actions.actions import Actions
        from csm.components.components import Components
        from csm.databases.databaseproviders import DatabaseProviders

        self.actions: List["Action"] = Registry.as_list(Actions)
        self.components: List["Component"] = Registry.as_list(Components)
        self.db_providers: List["DatabaseProvider"] = Registry.as_list(DatabaseProviders)
