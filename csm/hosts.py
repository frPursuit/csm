from typing import Dict, Optional

from pursuitlib.iterators import citer


class Hosts:
    HOSTS_FILE = "/etc/hosts"
    START_SIGNATURE = "##! CSM START"
    STOP_SIGNATURE = "##! CSM STOP"

    instance: "Hosts"

    @staticmethod
    def initialize():
        Hosts.instance = Hosts()

    def __init__(self):
        self.mapping = {}
        self.dirty = False

        with open(Hosts.HOSTS_FILE, "r") as file:
            found_section = False
            for line in file.readlines():
                line = line.replace("\n", "")  # Remove newline
                if found_section:
                    if line.strip().startswith(Hosts.STOP_SIGNATURE):
                        break
                    else:
                        items = list(citer(line.split(' ')).filter(lambda i: len(i) > 1))
                        if len(items) < 2:
                            continue
                        address = items[0]
                        hostname = items[1]
                        self.mapping[hostname] = address
                elif line.strip().startswith(Hosts.START_SIGNATURE):
                    found_section = True

    def set(self, hostname: str, address: str):
        self.mapping[hostname] = address
        self.dirty = True

    def get(self, hostname: str) -> Optional[str]:
        return self.mapping.get(hostname, None)

    def delete(self, hostname: str):
        if hostname in self.mapping:
            del self.mapping[hostname]
            self.dirty = True

    def get_mapping(self) -> Dict[str, str]:
        return self.mapping

    def is_dirty(self) -> bool:
        return self.dirty

    def save(self):
        before = []
        after = []

        lines = []
        if len(self.mapping) > 0:
            lines.append(f"{Hosts.START_SIGNATURE}\n")
            for hostname, address in self.mapping.items():
                lines.append(f"{address} {hostname}\n")
            lines.append(f"{Hosts.STOP_SIGNATURE}\n")

        with open(Hosts.HOSTS_FILE, "r") as file:
            step = 0
            for line in file.readlines():
                if step == 0:
                    if line.strip().startswith(Hosts.START_SIGNATURE):
                        step += 1
                    else: before.append(line)
                elif step == 1:
                    if line.strip().startswith(Hosts.STOP_SIGNATURE):
                        step += 1
                else: after.append(line)

        with open(Hosts.HOSTS_FILE, "w") as file:
            file.writelines(before)
            file.writelines(lines)
            file.writelines(after)

        self.dirty = False
