from typing import Optional

from csm.ipam.ipaddress import IPAddress
from csm.ipam.ipallocation import IPAllocation
from csm.ipam.ipprefix import IPPrefix


class IPNetwork(IPAllocation):
    def __init__(self, prefix: IPPrefix):
        super().__init__(prefix)

        # The first address is the network IP, and the last address is the broadcast IP
        self.mark_allocated(prefix.get_first_address(), prefix.get_last_address())

    def get_address(self) -> IPAddress:
        return self.get_pool().get_first_address(self.get_size())

    def get_size(self) -> int:
        return self.get_pool().get_size()

    def allocate_address(self, netmask: Optional[int] = None) -> IPAddress:
        if netmask is None:
            netmask = self.get_pool().get_size()
        return super().allocate_address(netmask)

    def __str__(self) -> str:
        return str(self.get_pool())
