from typing import Union, List

from csm.ipam.ipaddress import IPAddress
from csm.ipam.ipprefix import IPPrefix


class IPAllocation:
    def __init__(self, pool: IPPrefix):
        self.pool = pool
        self.allocated: List[IPPrefix] = []  # This array should always be sorted

    def mark_allocated(self, *items: Union[IPAddress, IPPrefix]):
        for item in items:
            if isinstance(item, IPAddress):
                item = IPPrefix(item.get_address_bytes(), 32)
            if item not in self.pool:
                raise ValueError(f"Cannot allocate {item} in {self.pool}: prefix not in pool")
            if item in self.allocated:
                raise ValueError(f"Cannot allocated {item} in {self.pool}: already allocated")
            self.allocated.append(item)
        self.allocated.sort(key=lambda p: p.get_first_address())

    def allocate_prefix(self, size: int) -> IPPrefix:
        if size < self.pool.get_size():
            raise ValueError(f"Unable to allocate prefix of size {size} inside {self.pool}")

        # Try to add the prefix at the beginning of the pool
        prefix = IPPrefix(self.pool.get_address_bytes(), size)

        if len(self.allocated) == 0 or not self.allocated[0].collides_with(prefix):
            self.allocated.insert(0, prefix)
            return prefix

        # Try to add the prefix after an existing prefix in the pool
        for i in range(len(self.allocated)):
            existing = self.allocated[i]
            prefix = existing.get_prefix_after(size)

            if existing.collides_with(prefix):
                continue
            if i+1 < len(self.allocated) and self.allocated[i+1].collides_with(prefix):
                continue
            if prefix not in self.pool:
                raise ValueError(f"Unable to allocate prefix of size {size} inside {self.pool}")

            self.allocated.insert(i + 1, prefix)
            return prefix

        raise ValueError(f"Unable to allocate prefix of size {size} inside {self.pool}")

    def allocate_address(self, netmask: int) -> IPAddress:
        prefix = self.allocate_prefix(32)
        return IPAddress(prefix.get_address_bytes(), netmask)

    def free(self, *items: Union[IPPrefix, IPAddress]):
        for item in items:
            if isinstance(item, IPAddress):
                item = IPPrefix(item.get_address_bytes(), 32)
            self.allocated.remove(item)

    def get_pool(self) -> IPPrefix:
        return self.pool

    def get_allocated(self) -> List[IPPrefix]:
        return self.allocated
