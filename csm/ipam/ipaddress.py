from typing import Tuple


class IPAddress:
    COMPARE_EXCEPTION = "Cannot compare an IPAddress with something that isn't an IPAddress"

    @staticmethod
    def parse(address: str, netmask: str) -> "IPAddress":
        address_bytes = IPAddress.parse_address(address)
        netsize = IPAddress.parse_network_mask(netmask)
        return IPAddress(address_bytes, netsize)

    @staticmethod
    def parse_cidr(value: str) -> "IPAddress":
        if '/' not in value:
            value = f"{value}/32"

        index = value.index('/')
        address = IPAddress.parse_address(value[:index])
        netsize = int(value[index+1:])
        return IPAddress(address, netsize)

    @staticmethod
    def parse_address(value: str) -> Tuple[int, int, int, int]:
        address_bytes = []
        parts = value.split('.')
        if len(parts) != 4:
            raise ValueError(f"Invalid IP address: {value}")

        for part in parts:
            address_bytes.append(int(part))
        return address_bytes[0], address_bytes[1], address_bytes[2], address_bytes[3]

    @staticmethod
    def parse_network_mask(value: str) -> int:
        mask_bytes = IPAddress.parse_address(value)
        size = 0
        done = False
        for byte in mask_bytes:
            for i in range(8):
                is_bit_set = ((byte >> (7 - i)) & 0b1) == 0b1
                if is_bit_set:
                    if done:
                        raise ValueError(f"Invalid network mask: {value}")
                    size += 1
                elif not done:
                    done = True
        return size

    def __init__(self, address: Tuple[int, int, int, int], netsize: int):
        self.address = address
        self.netsize = netsize

        for byte in address:
            if byte < 0 or byte > 255:
                raise ValueError(f"Invalid IP address: {self}")

    def get_address_bytes(self) -> Tuple[int, int, int, int]:
        return self.address

    def get_address(self) -> str:
        return ".".join([str(byte) for byte in self.address])

    def get_network_size(self) -> int:
        return self.netsize

    def get_network_mask(self) -> str:
        address_bytes = []
        for i in range(4):
            byte = 0
            for j in range(8):
                if self.netsize <= i * 8 + j:
                    break
                byte += 2**(7-j)
            address_bytes.append(byte)
        return ".".join([str(byte) for byte in address_bytes])

    def get_cidr(self) -> str:
        return f"{self.get_address()}/{self.get_network_size()}"

    def with_network_size(self, netsize: int) -> "IPAddress":
        return IPAddress(self.get_address_bytes(), netsize)

    def previous(self, netsize: int = 32) -> "IPAddress":
        address_bytes = list(self.get_address_bytes())
        for i in range(4):
            if address_bytes[3-i] <= 0:
                address_bytes[3 - i] = 255
                continue
            address_bytes[3-i] -= 1
            break
        return IPAddress((address_bytes[0], address_bytes[1], address_bytes[2], address_bytes[3]), netsize)

    def next(self, netsize: int = 32) -> "IPAddress":
        address_bytes = list(self.get_address_bytes())
        for i in range(4):
            if address_bytes[3-i] >= 255:
                address_bytes[3 - i] = 0
                continue
            address_bytes[3-i] += 1
            break
        return IPAddress((address_bytes[0], address_bytes[1], address_bytes[2], address_bytes[3]), netsize)

    def __eq__(self, other) -> bool:
        if not isinstance(other, IPAddress):
            raise ValueError(IPAddress.COMPARE_EXCEPTION)
        for i in range(len(self.address)):
            if self.address[i] != other.address[i]:
                return False
        return True

    def __ge__(self, other) -> bool:
        if not isinstance(other, IPAddress):
            raise ValueError(IPAddress.COMPARE_EXCEPTION)
        for i in range(len(self.address)):
            if self.address[i] != other.address[i]:
                return self.address[i] > other.address[i]
        return True

    def __le__(self, other) -> bool:
        if not isinstance(other, IPAddress):
            raise ValueError(IPAddress.COMPARE_EXCEPTION)
        for i in range(len(self.address)):
            if self.address[i] != other.address[i]:
                return self.address[i] < other.address[i]
        return True

    def __ne__(self, other) -> bool:
        return not (self == other)

    def __lt__(self, other) -> bool:
        return not (self >= other)

    def __gt__(self, other) -> bool:
        return not (self <= other)

    def __str__(self) -> str:
        return self.get_cidr()
