from typing import Tuple

from csm.ipam.ipaddress import IPAddress


class IPPrefix:
    COMPARE_EXCEPTION = "Cannot compare an IPPrefix with something that isn't an IPPrefix"

    @staticmethod
    def parse(address: str, netmask: str) -> "IPPrefix":
        address_bytes = IPAddress.parse_address(address)
        netsize = IPAddress.parse_network_mask(netmask)
        return IPPrefix(address_bytes, netsize)

    @staticmethod
    def parse_cidr(value: str) -> "IPPrefix":
        if '/' not in value:
            raise ValueError("An IP prefix should have an explicit size when using CIDR notation")

        index = value.index('/')
        address = IPAddress.parse_address(value[:index])
        netsize = int(value[index+1:])
        return IPPrefix(address, netsize)

    def __init__(self, address: Tuple[int, int, int, int], size: int):
        # Represents the first IP address and the size of this prefix
        self.address = IPAddress(address, size)

        for i, byte in enumerate(address):
            if byte < 0 or byte > 255:
                raise ValueError(f"Invalid IP prefix: {self}")
            for j in range(8):
                is_bit_set = ((byte >> (7 - j)) & 0b1) == 0b1
                if is_bit_set and (i*8 + j) >= size:
                    raise ValueError(f"Invalid IP prefix: {self}")

    def get_address_bytes(self) -> Tuple[int, int, int, int]:
        return self.address.get_address_bytes()

    def get_address(self) -> str:
        return self.address.get_address()

    def get_size(self) -> int:
        return self.address.get_network_size()

    def get_first_address(self, netsize: int = 32) -> IPAddress:
        return IPAddress(self.get_address_bytes(), netsize)

    def get_last_address(self, netsize: int = 32) -> IPAddress:
        address_bytes = self.get_address_bytes()
        offset = [0, 0, 0, 0]
        size = self.get_size()

        for idx in range(32-size):
            i = idx // 8
            j = idx % 8
            offset[3-i] += 2 ** j

        return IPAddress((address_bytes[0] + offset[0], address_bytes[1] + offset[1], address_bytes[2] + offset[2], address_bytes[3] + offset[3]), netsize)

    def get_first_address_after(self, netsize: int = 32) -> IPAddress:
        return self.get_last_address().next(netsize)

    def get_prefix_after(self, size: int) -> "IPPrefix":
        if size >= self.get_size():
            # If the netmask if bigger, we return a subset of the next prefix of the same size
            first_address = self.get_first_address_after()
            return IPPrefix(first_address.get_address_bytes(), size)
        else:
            # If the netmask is bigger, we return the prefix following an overall prefix of this bigger size
            return self.with_size(size).get_prefix_after(size)

    def collides_with(self, prefix: "IPPrefix") -> bool:
        self_start = self.get_first_address()
        self_end = self.get_first_address_after()
        other_start = prefix.get_first_address()
        other_end = prefix.get_first_address_after()
        return other_end > self_start and other_start < self_end

    def with_size(self, size: int) -> "IPPrefix":
        if size >= self.get_size():
            return IPPrefix(self.get_address_bytes(), size)
        else:
            # Apply the network mask
            address_bytes = self.get_address_bytes()
            offset = [0, 0, 0, 0]
            for i, byte in enumerate(address_bytes):
                for j in range(8):
                    is_bit_set = ((byte >> (7 - j)) & 0b1) == 0b1
                    if is_bit_set and (i * 8 + j) >= size:
                        offset[i] = 2 ** (7 - j)
            return IPPrefix((address_bytes[0] - offset[0], address_bytes[1] - offset[1], address_bytes[2] - offset[2], address_bytes[3] - offset[3]), size)

    def __contains__(self, item) -> bool:
        if isinstance(item, IPAddress):
            return self.get_first_address() <= item < self.get_first_address_after()
        if isinstance(item, IPPrefix):
            return item.get_first_address() in self and item.get_size() >= self.get_size()
        return False

    def __eq__(self, other) -> bool:
        if not isinstance(other, IPPrefix):
            return False
        return self.address == other.address

    def __str__(self) -> str:
        return str(self.address)
