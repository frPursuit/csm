import os.path


class Directories:
    PREFIX = ""
    INSTALL = f"{PREFIX}/usr/lib/csm"
    CONFIG = f"{PREFIX}/etc/csm"
    DATA = f"{PREFIX}/var/lib/csm"

    SERVICES_INSTALL = os.path.join(INSTALL, "services")
    SERVICES_CONFIG = os.path.join(CONFIG, "services")
    SERVICES_DATA = os.path.join(DATA, "services")
